<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="row col-md-8 col-md-offset-1 managing">

<c:forEach var="item" items="${tags}" >
    <spring:url value="/updatetag" var="updateTagUrl"/>
    <forms:form id="tag" name="tag" cssclass="form-horizontal" modelAttribute="tag" method="post" action="${updateTagUrl}">
        <div class="row col-md-16">
                <div class="form-group row col-md-12">
                    <div class="col-md-8">
                         <forms:input id="name${item.tagId}" type="text" path="name" value="${item.name}" class="form-control" disabled="true"/>
                            <c:if test="${tag.tagId eq item.tagId}">
                                <div class="col-md-14">
                                        <div class="alert alert-${css}">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>${statusMsg}</strong>
                                                <forms:errors path="name" cssClass="control-label" />
                                                ${msg}
                                        </div>
                                </div>
                            </c:if>
                        <forms:input path="tagId" type="hidden" value="${item.tagId}" />
                    </div>
                    <div>
                        <input id="edit${item.tagId}" class="btn btn-info col-md-1 col-md-offset 1 " type="button" onclick="showButtons(this)" value="Edit">
                        <div class="btn-group col-md-4">
                        <input id="save${item.tagId}" class="btn btn-warning hidden"  type="submit" value="Save" formaction="${pageContext.request.contextPath}/updatetag" formmethod="post">
                        <input id="delete${item.tagId}" class="btn btn-danger hidden" type="submit" value="Delete" formaction="${pageContext.request.contextPath}/deletetag/${item.tagId}" formmethod="post"/>
                        <input id="cancel${item.tagId}" class="btn btn-success hidden" type="button" value="Cancel" onclick="hideButtons(this)">
                        </div>
                        </div>
                </div>
        </div>
    </forms:form>
</c:forEach>
<div class="add">
    <spring:url value="/addtag" var="addTagUrl"/>
    <forms:form id="addTag" name="addTag" cssclass="form-horizontal" modelAttribute="addTag" method="post" action="${addTagUrl}">
        <div class="row col-md-16" >
            <div class="col-md-2" style="padding-left: 16px">
                <label for="name" >Add new Tag</label></div>
            <div class="form-group  row col-md-12">
            <div class="col-md-8">
                    <forms:input id="name" type="text" path="name" class="form-control"/>
                <c:if test="${add}">
                <div class="col-md-14">
                        <div class="alert alert-${css}">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>${statusMsg}</strong>
                                <forms:errors path="name" cssClass="control-label" />
                                ${msg}
                        </div>
                </div>
                </c:if>

                </div>
                <div><input class="btn btn-info" type="submit" value="Add tag"></div>
                </div>
        </div>
    </forms:form>
</div>
</div>
<spring:url value="/resources/js/custom/tags.js" var="tags"/>
<script type='text/javascript' src="${tags}"></script>
