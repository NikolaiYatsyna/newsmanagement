<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div>
    <spring:url value="/addnews" var="action" />
    <c:if test="${not empty addNewsTO.news}">
        <spring:url value="/editNews/${addNewsTO.news.newsId}" var="action" />
        <c:if test="${not empty error}" >
        <div class="row col-md-offset-2 col-md-6 alert alert-danger " style="padding-top: 10px;">
                ${error}
        </div>
        </c:if>
    </c:if>



    <div class="row col-md-10" style="padding-top: 10px;">
        <forms:form name="addnews" modelAttribute="addNewsTO" cssclass="form-horizontal" action="${action}" method="post">
            <forms:hidden path="news.newsId"/>
            <spring:bind path="news.title">
                <div class="row form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label" >Title</label>
                    <div class="col-sm-10">
                        <forms:input path="news.title" type="text" class="form-control " id="title" placeholder="Title" value="${addNewsTO.news.title}"/>
                        <forms:errors path="news.title" cssClass="control-label"/>
                        </div>
                </div>
            </spring:bind>
            <c:if test="${not empty addNewsTO.news.creationDate}">
                <spring:bind path="news.creationDate" >
                    <forms:input path="news.creationDate" type="hidden" value="${addNewsTO.news.creationDate}" />
                </spring:bind>
            </c:if>
            <p/>

            <spring:bind path="news.version" >
                <forms:input path="news.version" type="hidden" value="${addNewsTO.news.version}" />
            </spring:bind>
            <spring:bind path="news.shortText">
                <div class="row form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label" >Short Text</label>
                    <div class="col-sm-10">
                        <forms:input path="news.shortText" type="text" class="form-control " id="shortText" placeholder="Short Text" value="${addNewsTO.news.shortText}"/>
                        <forms:errors path="news.shortText" cssClass="control-label"/>
                    </div>
                </div>
            </spring:bind>
            <p/>
            <spring:bind path="news.fullText">
                <div class="row form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label" >Full Text</label>
                    <div class="col-sm-10">
                        <forms:textarea path="news.fullText" type="text-area" rows="7" size="2000" maxlength="2000" class="form-control " id="title" placeholder="Full Text" value="${addNewsTO.news.fullText}"/>
                        <forms:errors path="news.fullText" cssClass="control-label"/>
                    </div>
                </div>
            </spring:bind>
            <spring:bind path="authors">
            <div class=" row form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label" >Select author</label>
                <div class="col-sm-10">
                        <forms:select class="form-control" path="authors">
                            <c:forEach items="${authorList}" var="author">
                                <forms:option value="${author.getAuthorId() }">
                                    <c:out value="${author.getName()}" />
                                </forms:option>
                            </c:forEach>
                        </forms:select>
                    </div>

            </div>
            </spring:bind>
            <spring:bind path="tags">
            <div class="row form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label" >Select tags</label>
                <div class="col-sm-10">

                    <forms:select class="form-control" path="tags" multiple="multiple">
                        <c:forEach items="${tagList}" var="tag">
                            <forms:option value="${tag.getTagId() }">
                                <c:out value="${tag.getName()}" />
                            </forms:option>
                        </c:forEach>
                    </forms:select>
                </div>
            </div>
            </spring:bind>
            <div class="row ">
                <input type="submit" class="col-md-1 col-md-offset-5 btn btn-success pull-left" value="Save">
            <forms:input path="news" type="reset" class="btn btn-danger col-md-1 col-md-offset-1" value="Reset"/>
            </div>
        </forms:form>
    </div>
</div>