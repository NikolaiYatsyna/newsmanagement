<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row col-md-8 col-md-offset-1 managing">

    <c:forEach var="item" items="${authors}" >
        <spring:url value="/updateauthor" var="updateAuthorURL"/>
        <forms:form cssclass="form-horizontal" modelAttribute="author" method="post" action="${updateAuthorURL}">
            <div class="row col-md-16">
                <spring:bind path="name">
                    <div class="form-group row col-md-12">
                        <div class="col-md-6">
                            <forms:input id="name${item.authorId}" type="text" path="name" value="${item.name}" class="form-control"  disabled="true"/>
                            <c:if test="${author.authorId eq item.authorId}">
                                <div class="col-md-14">
                                    <div class="alert alert-${css}">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>${statusMsg}</strong>
                                        <forms:errors path="name" cssClass="control-label" />
                                            ${msg}
                                    </div>
                                </div>
                            </c:if>
                            <forms:input path="authorId" type="hidden" value="${item.authorId}"/>
                        </div>
                </spring:bind>
            </forms:form>
                        <div>
                            <input id="edit${item.authorId}" class="btn btn-info col-md-1 col-md-offset 1" type="button" onclick="showButtons(this)" value="Edit">
                            <div class="btn-group col-md-5">
                                <input class="btn btn-warning hidden" id="save${item.authorId}"  type="submit" value="Save">
                                <div class="expired col-md-5 hidden" id="expired${item.authorId}"  >
                                    <c:set var="state" value="${item.expired}"/>
                                    <c:if test="${state == null}">
                                        <forms:form method="POST" modelAttribute="author" action="${pageContext.request.contextPath}/expiredAuthor">
                                            <forms:input type="hidden" path="authorId" value="${item.authorId}"/>
                                            <forms:input type="hidden" path="name" value="${item.name}"/>
                                            <input type="submit" class="btn btn-danger" value="Expired">
                                        </forms:form>
                                    </c:if>
                                    <c:if test="${state != null}">
                                        <p>Expired date: <fmt:formatDate type="both"
                                                                         dateStyle="short" timeStyle="short"
                                                                         value="${item.expired}"/></p>
                                    </c:if>
                                </div>
                                <input class="btn btn-success   hidden" id="cancel${item.authorId}" type="button" value="Cancel" onclick="hideButtons(this)">
                            </div>
                        </div>
                    </div>


            </div>

    </c:forEach>
    <div class="add">
        <spring:url value="/addauthor" var="addAuthorUrl"/>
        <forms:form cssclass="form-horizontal" modelAttribute="addAuthor" method="post" action="${addAuthorUrl}">
            <spring:bind path="name">
                <forms:input path="authorId" type="hidden"/>
                <div class="row col-md-16">
                    <div class="col-md-2" style="padding-left: 16px">
                        <label for="name" >Add new Author</label></div>
                    <div class="form-group row col-md-12">
                        <div class="col-md-6">
                            <forms:input path="authorId" type="hidden" />
                            <forms:input type="text" path="name" class="form-control"/>
                            <div class="col-md-14">
                                <c:if test="${add}">
                                    <div class="col-md-14">
                                        <div class="alert alert-${css}">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>${statusMsg}</strong>
                                            <forms:errors path="name" cssClass="control-label" />
                                                ${msg}
                                        </div>

                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div><input class="btn btn-info" type="submit" value="Add author"></div>
                    </div>
                </div>

            </spring:bind>
        </forms:form>
    </div>
</div>
<spring:url value="/resources/js/custom/authors.js" var="authors"/>
<script type='text/javascript' src="${authors}"></script>
