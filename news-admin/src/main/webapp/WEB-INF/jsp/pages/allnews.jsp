<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row col-md-9" style="padding-left: 50px">
    <div class="row col-md-16">
        <div class="col-md-0"></div>
        <sf:form class="form" method="POST" modelAttribute="searchCriteria"
                 action="${pageContext.request.contextPath}/searchresult?page=1">
            <div class="col-md-5">
                <sf:select class="form-control" path="authorId" required="required" multiple="multiple">
                    <sf:option value="">Select author</sf:option>
                    <c:forEach items="${authorList}" var="author">
                        <sf:option value="${author.getAuthorId() }">
                            <c:out value="${author.getName()}" />
                        </sf:option>
                    </c:forEach>
                </sf:select>
            </div>
            <div class="col-md-5">
                <sf:select class="form-control" path="tagsId" multiple="multiple">
                    <sf:option value="">Select tags</sf:option>
                    <c:forEach items="${tagList}" var="tag">
                        <sf:option value="${tag.getTagId() }">
                            <c:out value="${tag.getName()}" />
                        </sf:option>
                    </c:forEach>
                </sf:select>
            </div>
            <input class="btn btn-info" type="submit" value="Filter" />

        </sf:form>
        <sf:form  method="POST" action="${pageContext.request.contextPath}/deleteCriteria">
            <input class="btn btn-danger" type="submit"  value="Reset"/>
        </sf:form>
    </div>
    <p class="lead">List of News (use checkboxes for news deletion)</p>
    <div class="col-md-10">
        <sf:form cssClass="form-horizontal" method="post" action="${pageContext.request.contextPath}/deleteNews" modelAttribute="deleteNewsTO">
            <p></p>
        <c:forEach var="item" items="${newsTO}">
            <div class="row news">
                <div class="col-md-1">
                <p><sf:checkbox path="deleteNewsList" value="${item.news.newsId}"/>
            </div>
                <div class="col-md-16 col-md-offset-1 ">
                    <div class="row">
                        <a href="${pageContext.request.contextPath}/news/${item.news.newsId}"><p class="text-left">${item.news.title}</p></a>
                    </div>
                    <div class="row col-md-14">
                        <div class="row">
                            <c:forEach items="${item.authorList}" var="author">
                                <div class="col-md-4"><p class="glyphicon glyphicon-user"> ${author.name}</p></div>
                            </c:forEach>
                            <div class="col-md-6 col-md-offset-1">
                                <p class="text-right glyphicon glyphicon-calendar"> Creation date : <fmt:formatDate dateStyle="short" value="${item.news.creationDate}"/></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <p class="glyphicon glyphicon-tags">
                            <c:forEach var="tag" items="${item.tagList}">
                                <span>${tag.name};</span>
                            </c:forEach>
                        </p>
                    </div>
                    <div class="row col-md-14">
                        <p>${item.news.shortText}</p>
                    </div>

                    <div class="row col-md-14">
                        <div><p class="glyphicon glyphicon-comment"> ${fn:length(item.commentList)} Comments</p></div>

                    </div>
                    <div class="row">

                        <a href="${pageContext.request.contextPath}/editNews/${item.news.newsId}" class="btn btn-default pull-left">Edit</a>
                        <a href="${pageContext.request.contextPath}/news/${item.news.newsId}" class="btn btn-default pull-right">More details</a>
                    </div>
                </div>
            </div>
        </c:forEach>
            <div class="deletebutton btn-group">
                <input type="button" onclick="setCheckboxes()" class="btn btn-info btn-warning" value="Select all">
                <input type="button" onclick="clearCheckboxes()" class="btn btn-info btn-success" value="Unselect all">
                <input type="submit" class="btn btn-info btn-danger" value="Delete checked">
            </div>
    </sf:form>
    </div>
        <div class="col-md-6 col-md-offset-2 pagination-centered">
            <ul id="pagination-demo" class="pagination">
                <li><a href="${pageContext.request.contextPath}/allnews?page=1">&laquo;</a></li>
                <input type="hidden" value="${pageCount}" id="pageCount">
                <c:forEach var="pageNumber" end="${pageCount}" begin="1">
                    <li><a href="${pageContext.request.contextPath}/allnews?page=${pageNumber}">${pageNumber}</a></li>
                </c:forEach>
                <li><a href="${pageContext.request.contextPath}/allnews?page=${pageCount}">&raquo;</a></li>
            </ul>
    </div>
</div>
</div>
<spring:url value="/resources/js/jquery-2.1.3.min.js" var="jquery"/>
<script type='text/javascript' src="${jquery}"></script>
<spring:url value="/resources/js/custom/news-pagination.js" var="pagination"/>
<script type='text/javascript' src="${pagination}" ></script>
<spring:url value="/resources/js/jquery.twbsPagination.min.js" var="jQueryPagination"/>
<script type='text/javascript' src="${jQueryPagination}" ></script>
<spring:url value="/resources/js/custom/ckeckboxes.js" var="news"/>
<script type='text/javascript' src="${news}"></script>

