<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <div class="row col-md-9" style="padding-left: 50px">
        <div>
            <div class="row col-md-16">
                <div class="row">
                    <p class="text-left">${newsTO.news.title}</p>
                </div>
                <div class="row col-md-14">
                    <div class="row">
                        <c:forEach items="${newsTO.authorList}" var="author">
                            <div class="col-md-2"><p class="glyphicon glyphicon-user"> ${author.name}</p></div>
                        </c:forEach>
                        <div class="col-md-6 col-md-offset-4">
                            <p class="text-right glyphicon glyphicon-calendar "> Creation date : <fmt:formatDate dateStyle="short" value="${newsTO.news.creationDate}"/></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p>
                    <p class="glyphicon glyphicon-tags">
                        <c:forEach var="tag" items="${newsTO.tagList}">
                            <span>${tag.name};</span>
                        </c:forEach>
                    </p>
                </div>
                <div class="row col-md-14">
                    <p>${newsTO.news.fullText}</p>
                </div>
                <form:form  method="post" action="${pageContext.request.contextPath}/deletecomments/${comment.news.newsId}" cssClass="form-horizontal" modelAttribute="deleteCommentTO">
               <c:if test="${not empty newsTO.commentList}"> <div class="row col-md-14">
                    <p class="text-right"><div class="text">Comments:</div></p>
                    <c:forEach var="item" items="${newsTO.commentList}">
                <div class="row col-md-14">
                    <div class="row col-md-14">

                        <form:checkbox path="deleteCommentList" value="${item.commentId}" cssClass="pull-left"/>
                        <div class="col-md-2" >Check for delete</div>
                    </div>
                    <div class="col-md-12">
                    <p class="text">
                    <div class="text-left">
                        <p class="glyphicon glyphicon-calendar ">
                        <fmt:formatDate dateStyle="short" value="${item.creationDate}"/>
                    </div>
                    <div class="text-left">${item.text}</div>
                    </div>
                    </p>
                </div>
            </c:forEach>
                </div></c:if>
                    <div class="row"><input type="submit" class="btn btn-success" value="Delete Selected Comments" ></div>
                </form:form>
                <div class="row">
                    <form:form cssClass="form-horizontal" method="post" action="${pageContext.request.contextPath}/postcomment" modelAttribute="comment">
                        <form:input path="news.newsId" type = "hidden" value="${newsTO.news.newsId}"/>
                        <label>Post comment:</label>
                        <form:input path="text" type="text-area" cssClass="form-control" rows="5" cols="55"/>
                        <input type="submit" class="btn btn-success" value="Post comment">
                        <div class="col-md-14">
                            <c:if test="${not empty statusMsg}">
                                <div class="alert alert-${css}">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>${statusMsg}</strong>
                                    <form:errors path="text" cssClass="control-label" />
                                        ${msg}
                                </div>
                            </c:if>
                        </div>
                    </form:form>

                    <div class="row col-md-14">
                        <ul class="pager">
                            <li class="previous">
                                <a href="${pageContext.request.contextPath}/news/${previous}">&larr; Previous</a>
                            </li>
                            <li class="next">
                                <a href="${pageContext.request.contextPath}/news/${next}">Next &rarr;</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

    </div>
    </div>