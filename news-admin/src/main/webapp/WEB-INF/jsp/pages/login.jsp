<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="login-form page-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div>
                        <div>
                            <h3>Login to News Management Administration Portal</h3>
                            <p>Enter your username and password to log on:</p>
                        </div>
                    </div>
                    <div>
                        <form role="form" action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Username</label>
                                <input type="text" name="j_username" placeholder="Username..." class="form-username form-control" id="form-username" >
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">Password</label>
                                <input type="password" name="j_password" placeholder="Password..." class="form-password form-control" id="form-password" >
                            </div>
                            <button type="submit" class="btn">Sign in!</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>