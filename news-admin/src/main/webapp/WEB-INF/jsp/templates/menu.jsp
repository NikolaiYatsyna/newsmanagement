<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-md-2">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation">
            <spring:url value="/allnews?page=1" var="allNews"/>
            <a href="${allNews}">All News</a>
        </li> 
        <li role="presentation">
            <spring:url value="/addnews" var="addnews"/>
            <a href="${addnews}">Add News</a>
        </li> 
        <li role="presentation">
            <spring:url value="/editauthor" var="editauthor"/>
            <a href="${editauthor}">Authors managing</a>
        </li>
        <li role="presentation">
            <spring:url value="/edittags" var="edittags"/>
            <a href="${edittags}">Tags managing </a>
        </li>
    </ul>
</div>
<spring:url value="/resources/js/custom/menu.js" var="menu"/>
<script type='text/javascript' src="${menu}"></script>