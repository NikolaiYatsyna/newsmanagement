<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
    <title><tiles:insertAttribute name="title"/> </title>
    <spring:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>

    <spring:url value="/resources/css/style.css" var="style"/>
    <link href="${style}" rel="stylesheet"/>
    </head>
<body>
<tiles:insertAttribute name="body" />
<tiles:insertAttribute name="footer"/>
</body>
</html>
