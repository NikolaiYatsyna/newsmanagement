<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<footer class="footer" id="footer">
    <div class="col-md-15">
        <p class = "text-muted text-center footer-text ">Mikalai Yatsyna. EPAM 2016</p>
    </div>
    <spring:url value="/resources/js/jquery-2.1.3.min.js" var="jquery"/>
    <script type='text/javascript' src="${jquery}"></script>

    <spring:url value="/resources/js/bootstrap.js" var="bootstrap"/>
    <script type='text/javascript' src="${bootstrap}"></script>
</footer>