<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title><tiles:insertAttribute name="title"/></title>

    <spring:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <spring:url value="/resources/css/style.css" var="style"/>
    <link href="${style}" rel="stylesheet"/>


</head>
<body>
<div class="error-container">
    <div class="error-container row-fluid">
        <div class="col-lg-12">
            <div class="centering text-center error-container">
                <div class="text-center">
                    <h2 class="without-margin">Don't worry. It's <span class="text-warning">
                    <big><tiles:insertAttribute name="error-code"/></big></span> error only.</h2>
                    <h4 class="text-warning"><tiles:insertAttribute name="error-message"/></h4>
                </div>
                <div class="text-center">
                    <h3><small>Choose an option below</small></h3>
                </div>
                <hr>
                <ul class="pager">
                    <li><a href="${pageContext.request.contextPath}/<tiles:insertAttribute name="homeLink"/>">
                                <tiles:insertAttribute name="linkName" /></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div>
<tiles:insertAttribute name="footer"/>
</body>
</html>
