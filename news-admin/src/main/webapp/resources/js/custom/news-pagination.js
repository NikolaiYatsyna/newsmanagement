$(function(){
    var  pages = $('input#pageCount').val();
    $('#pagination-demo').twbsPagination({
        totalPages: pages,
        visiblePages: 2,
        href: '?page={{number}}',
        onPageClick: function (event, page) {
            $('#page-content').text('Page ' + page);
        }
    });
});