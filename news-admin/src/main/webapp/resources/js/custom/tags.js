var HIDDEN_CLASS = "hidden";
var SAVE_ID = "#save";
var EDIT_ID = "#edit";
var DELETE_ID = "#delete";
var CANCEL_ID = "#cancel";
var NAME_ID = "#name";
var DISABLED = 'disabled';
var defaultData = null;

function showButtons(object) {
    var tempId = object.id;
    var buttonId = parseInt(tempId.replace(/\D+/g,""));
    
    getDefaultData(buttonId);
    $(object).addClass(HIDDEN_CLASS);

    var saveId = SAVE_ID + buttonId;
    $(saveId).removeClass(HIDDEN_CLASS);

    var deleteId = DELETE_ID + buttonId;
    $(deleteId).removeClass(HIDDEN_CLASS);

    var cancelId = CANCEL_ID + buttonId;
    $(cancelId).removeClass(HIDDEN_CLASS);
    
    var nameId = NAME_ID + buttonId;
    $(nameId).removeAttr(DISABLED);
}

function hideButtons(object) {
    var tempId = object.id;
    var buttonId = parseInt(tempId.replace(/\D+/g,""));

    setDefaultData(buttonId);
    $(object).addClass(HIDDEN_CLASS);
    
    var saveId = SAVE_ID + buttonId;
    $(saveId).addClass(HIDDEN_CLASS);

    var deleteId = DELETE_ID + buttonId;
    $(deleteId).addClass(HIDDEN_CLASS);

    var editId = EDIT_ID + buttonId;
    $(editId).removeClass(HIDDEN_CLASS);

    var nameId = NAME_ID + buttonId;
    $(nameId).attr(DISABLED,DISABLED);
}

function getDefaultData(elementId) {
    var dataId = NAME_ID + elementId;
    defaultData = $(dataId).val();
}

function setDefaultData(elementId) {
    var dataId = NAME_ID + elementId;
    $(dataId).val(defaultData);
}