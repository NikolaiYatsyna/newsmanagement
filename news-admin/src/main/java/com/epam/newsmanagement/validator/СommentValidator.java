package com.epam.newsmanagement.validator;

import com.epam.newsmanagement.domain.bean.Comment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
@Component
public class СommentValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Comment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors,"text","Not.Empty.Comment.text","Comment text must be not empty String!");
        Comment comment = (Comment) target;
        if(comment.getNews()==null){
            errors.rejectValue("news","NotNull.Comment.News","News can't be null!");
        }
    }
}
