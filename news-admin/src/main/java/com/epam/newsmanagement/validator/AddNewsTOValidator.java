package com.epam.newsmanagement.validator;

import com.epam.newsmanagement.domain.to.AddNewsTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AddNewsTOValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return AddNewsTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"news.title","Not.Empty.News.Title","Title can'be empty string!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"news.shortText","Not.Empty.News.ShortText","Short text can'be empty string!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"news.fullText","Not.Empty.News.FullText","Full text can'be empty string!");
        AddNewsTO addNewsTO = (AddNewsTO) target;
        if(addNewsTO.getAuthors()==null || addNewsTO.getAuthors().isEmpty()){
            errors.rejectValue("authors","NotEmpty.Author","SelectAuthors!");
        }

        if(addNewsTO.getTags()==null || addNewsTO.getTags().isEmpty()){
            errors.rejectValue("tags","NotEmpty.Tags","Select Tags!");
        }
    }
}
