package com.epam.newsmanagement.util;

import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class RedirectAttributesBuilder {

    public static void buildErrorAttributes(RedirectAttributes redirectAttributes, BindingResult bindingResult,String modelAttributeName){
        final String CSS_DANGER = "danger";
        final String STATUS_ERROR ="Error";
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult."+modelAttributeName, bindingResult);
        buildRedirectAttributes(redirectAttributes,CSS_DANGER,STATUS_ERROR);
    }

    public static void buildSuccessAttributes(RedirectAttributes redirectAttributes,String message){
        final String MESSAGE_FLASH_ATTRIBUTE = "msg";
        final String CSS_SUCCESS = "success";
        final String STATUS_SUCCESS = "Success";
        buildRedirectAttributes(redirectAttributes, CSS_SUCCESS,STATUS_SUCCESS);
        redirectAttributes.addFlashAttribute(MESSAGE_FLASH_ATTRIBUTE,message);
    }

    private static void buildRedirectAttributes(RedirectAttributes redirectAttributes,String css,String status){

        final String CSS_FLASH_ATTRIBUTE = "css";
        final String STATUS_FLASH_ATTRIBUTE = "statusMsg";
        redirectAttributes.addFlashAttribute(CSS_FLASH_ATTRIBUTE,css );
        redirectAttributes.addFlashAttribute(STATUS_FLASH_ATTRIBUTE,status);

    }
}
