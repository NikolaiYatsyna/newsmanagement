package com.epam.newsmanagement.util;

import com.epam.newsmanagement.exception.ServiceException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/*@ControllerAdvice(basePackages = {"com.epam.newsmanagement.controller"})*/
public class GlobalExceptionHandler {

       @ExceptionHandler(ServiceException.class)
       public ModelAndView serviceExceptionHandler(){
           final String SERVICE = "Service";
           ModelAndView modelAndView = new ModelAndView(SERVICE);
           return modelAndView;
       }

        @ExceptionHandler(Throwable.class)
        public ModelAndView notFound(){
            final String UNEXPECTED = "Unexpected";
            ModelAndView modelAndView = new ModelAndView(UNEXPECTED);
            return modelAndView;
        }
}
