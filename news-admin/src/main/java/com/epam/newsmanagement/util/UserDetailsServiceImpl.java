package com.epam.newsmanagement.util;

import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        com.epam.newsmanagement.domain.bean.User user = null;
        try {
            user = userService.findByLogin(s);
        } catch (ServiceException e) {
            LOGGER.warn("Error while executing service", e);
        }
        List<GrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(new SimpleGrantedAuthority(user.getRole().getName().toString().toUpperCase()));
        return buildUserForAuthentication(user,authorityList);
    }

    private User buildUserForAuthentication(com.epam.newsmanagement.domain.bean.User user,
                                            List<GrantedAuthority> authorities) {
        return new User(user.getLogin(),user.getPassword(), authorities);
    }

}
