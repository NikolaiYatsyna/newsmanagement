package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.to.AddNewsTO;
import com.epam.newsmanagement.domain.to.DeleteCommentTO;
import com.epam.newsmanagement.domain.to.DeleteNewsTO;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ComplexNewsService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.validator.AddNewsTOValidator;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class NewsController {

    @Autowired
    ComplexNewsService complexNewsService;
    @Autowired
    NewsService newsService;
    @Autowired
    AuthorService authorService;
    @Autowired
    TagService tagService;

    @Autowired
    AddNewsTOValidator newsValidator;

    @InitBinder(ADD_NEWS_MODEL)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(newsValidator);
    }

    private static final String ALL_NEWS_MAPPING = "/allnews";
    private static final String PAGE_NUMBER_VARIABLE = "page";
    private static final String NEWS_MAPPING ="/news/{id}" ;
    private static final String NEWS_ID_VARIABLE ="id" ;
    private static final String SEARCH_RESULT_MAPPING = "/searchresult";
    private static final String SEARCH_CRITERIA_VARIABLE ="searchCriteria" ;
    private static final String ADD_NEWS_MAPPING = "/addnews";
    private static final String REDIRECT_PREFIX = "redirect:";
    private static final String ADD_NEWS_MODEL = "addNewsTO";
    private static final String DELETE_NEWS_TO_ATTRIBUTE = "deleteNewsTO";
    private static final String DELETE_NEWS_MAPPING = "/deleteNews";
    private static final String EDIT_NEWS_MAPPING = "/editNews/{id}";
    private static final String TAG_ATTRIBUTE = "tagList";
    private static final String AUTHOR_ATTRIBUTE = "authorList";
    private static final String ADD_VIEW_NAME = "addnews";
    private static final String DELETE_COMMENT_TO = "deleteCommentTO";
    private static final String RESET_MAPPING = "/deleteCriteria";




    @RequestMapping(value = ALL_NEWS_MAPPING, method = RequestMethod.GET)
    public ModelAndView newsPage(ModelAndView modelAndView, @RequestParam(PAGE_NUMBER_VARIABLE) Long pageNumber) throws  ServiceException{
        final String VIEW_NAME = "allnews";
        if (pageNumber==null) {
            pageNumber =1L;
        }
        List<NewsTO> news = complexNewsService.findNewsTOPage(pageNumber);
        List<Author> authors = authorService.findAll();
        List<Tag> tags = tagService.findAll();
        Long newsCount = newsService.countOfNodes();
        fillModelAndView(modelAndView,news,authors,tags,newsCount,VIEW_NAME,pageNumber);
        return modelAndView;
    }

    @RequestMapping(value = NEWS_MAPPING,method = RequestMethod.GET)
    public ModelAndView news(@PathVariable(NEWS_ID_VARIABLE) Long id,final HttpSession session) throws ServiceException {
        final String VIEW_NAME = "news";
        final String COMMENT ="comment" ;
        final String NEWS_TO = "newsTO";
        final String PREVIOUS = "previous";
        final String NEXT = "next";
        NewsTO newsTO = complexNewsService.findNewsTO(id);
        ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        Comment comment = new Comment();
        comment.setNews(newsTO.getNews());
        modelAndView.addObject(COMMENT,comment);
        modelAndView.addObject(NEWS_TO,newsTO);
        modelAndView.addObject(DELETE_COMMENT_TO,new DeleteCommentTO());
        SearchCriteria criteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA_VARIABLE);
        if(criteria == null){
            modelAndView.addObject(PREVIOUS,complexNewsService.findPrevious(id));
            modelAndView.addObject(NEXT,complexNewsService.findNext(id));
        }else{
            modelAndView.addObject(PREVIOUS,complexNewsService.findPreviousByCriteria(id,criteria));
            modelAndView.addObject(NEXT,complexNewsService.findNextByCriteria(id,criteria));
        }
        return modelAndView;
    }


    @RequestMapping(value= SEARCH_RESULT_MAPPING ,method=RequestMethod.POST)
    public ModelAndView search(@ModelAttribute(SEARCH_CRITERIA_VARIABLE) SearchCriteria searchCriteria,
                               @RequestParam(PAGE_NUMBER_VARIABLE) Long pageNumber, HttpSession session) throws ServiceException {
        final String VIEW_NAME = "allnews";
        SearchCriteria criteria = null;
        if (pageNumber==null) {
            pageNumber =1L;
        }
        if(searchCriteria.getAuthorId() == null) {
            criteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA_VARIABLE);
        } else {
            session.removeAttribute(SEARCH_CRITERIA_VARIABLE);
            session.setAttribute(SEARCH_CRITERIA_VARIABLE, searchCriteria);
            criteria = searchCriteria;
        }
        ModelAndView modelAndView = new ModelAndView();
        List<NewsTO> news = complexNewsService.findNewsTOPageByCriteria(criteria,pageNumber);
        Long newsTOCount = newsService.countOfNodesByCriteria(criteria);
        List<Author> authors = authorService.findAll();
        List<Tag> tags = tagService.findAll();
        fillModelAndView(modelAndView,news,authors,tags,newsTOCount,VIEW_NAME,pageNumber);
        return modelAndView;
    }

    @RequestMapping(value = ADD_NEWS_MAPPING , method = RequestMethod.GET)
    public ModelAndView addNewsPage() throws ServiceException {
        final String VIEW_NAME = "addnews";
        final String NEWS_ATTRIBUTE = "addNewsTO";
        ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        modelAndView.addObject(NEWS_ATTRIBUTE,new AddNewsTO());
        modelAndView.addObject(TAG_ATTRIBUTE,tagService.findAll());
        modelAndView.addObject(AUTHOR_ATTRIBUTE,authorService.findNonExpired());
        return modelAndView;

    }

    @RequestMapping(value = ADD_NEWS_MAPPING,method = RequestMethod.POST)
    public ModelAndView addNews(@ModelAttribute(ADD_NEWS_MODEL) @Validated AddNewsTO addNewsTO, BindingResult bindingResult) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        final String EDIT_NEWS= "/news/";
        Long id;
        if(bindingResult.hasErrors()){
            modelAndView.setViewName(ADD_VIEW_NAME);
            modelAndView.addObject(TAG_ATTRIBUTE,tagService.findAll());
            modelAndView.addObject(AUTHOR_ATTRIBUTE,authorService.findNonExpired());
            return modelAndView;
        }else{
            id = complexNewsService.createNewsWithTagsAndAuthor(addNewsTO.getNews(),addNewsTO.getAuthors(),addNewsTO.getTags());
        }
            String REDIRECT_PATH = REDIRECT_PREFIX + EDIT_NEWS+ id;
            modelAndView.setViewName(REDIRECT_PATH);
        return modelAndView;
    }

    @RequestMapping (value = DELETE_NEWS_MAPPING , method = RequestMethod.POST)
    public ModelAndView deleteNews(@ModelAttribute(DELETE_NEWS_TO_ATTRIBUTE) DeleteNewsTO deleteNewsTO) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        final String ALL_NEWS_MAPPING = "/allnews?page=1";
        complexNewsService.deleteNewsWithTagsAndAuthor(deleteNewsTO);
        String REDIRECT_PATH = REDIRECT_PREFIX + ALL_NEWS_MAPPING;
        modelAndView.setViewName(REDIRECT_PATH);
        return modelAndView;

    }

    @RequestMapping(value = EDIT_NEWS_MAPPING , method = RequestMethod.GET)
    public ModelAndView editNewsPage(@PathVariable(NEWS_ID_VARIABLE) Long id) throws ServiceException {
        final String VIEW_NAME = "editNews";
        final String NEWS_ATTRIBUTE = "addNewsTO";
        final String TAG_ATTRIBUTE = "tagList";
        final String AUTHOR_ATTRIBUTE = "authorList";
        ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        NewsTO newsTO = complexNewsService.findNewsTO(id);
        AddNewsTO addNewsTO = new AddNewsTO();
        addNewsTO.setNews(newsTO.getNews());
        modelAndView.addObject(NEWS_ATTRIBUTE, addNewsTO);
        modelAndView.addObject(TAG_ATTRIBUTE,tagService.findAll());
        modelAndView.addObject(AUTHOR_ATTRIBUTE,authorService.findNonExpired());
        return modelAndView;

    }

    @RequestMapping(value = EDIT_NEWS_MAPPING , method = RequestMethod.POST)
    public ModelAndView editNews(@ModelAttribute(ADD_NEWS_MODEL) @Validated AddNewsTO addNewsTO, BindingResult bindingResult,RedirectAttributes redirectAttributes) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        final String EDIT_NEWS= "/news/";
        if(bindingResult.hasErrors()){
            return editNewsPage(addNewsTO.getNews().getNewsId());
        }else {
            try {
                complexNewsService.updateNews(addNewsTO.getNews(),addNewsTO.getAuthors(),addNewsTO.getTags());
            } catch (StaleObjectStateException | OptimisticLockException e) {
                redirectAttributes.addFlashAttribute("error","This news have been edited by another user!");
                redirectAttributes.addFlashAttribute(ADD_NEWS_MODEL,addNewsTO);
                String REDIRECT_PATH = REDIRECT_PREFIX +"/editNews/" + addNewsTO.getNews().getNewsId();
                modelAndView.setViewName(REDIRECT_PATH);
                return modelAndView;
            }

        }
        redirectAttributes.addFlashAttribute(ADD_NEWS_MODEL,addNewsTO);
        String REDIRECT_PATH = REDIRECT_PREFIX + EDIT_NEWS + addNewsTO.getNews().getNewsId();
        modelAndView.setViewName(REDIRECT_PATH);
        return modelAndView;
    }

    @RequestMapping(value = RESET_MAPPING, method = RequestMethod.POST)
    public ModelAndView resetCriteria(final HttpSession session){
        final String ALL_NEWS_MAPPING = "/allnews?page=1";
        session.removeAttribute(SEARCH_CRITERIA_VARIABLE);
        ModelAndView modelAndView  = new ModelAndView();
        String path = REDIRECT_PREFIX + ALL_NEWS_MAPPING;
        modelAndView.setViewName(path);
        return modelAndView;
    }

    private void fillModelAndView(ModelAndView modelAndView,List<NewsTO> news,List<Author> authors,
                                  List<Tag> tags,Long newsCount,String viewName,Long pageNumber){
        final String SEARCH_CRITERIA = "searchCriteria";
        final String NEWS_TO_DELETE = "deleteNewsTO";
        final String NEWS_TO = "newsTO";
        final String AUTHOR_LIST = "authorList";
        final String TAG_LIST = "tagList";
        final String PAGE_COUNT = "pageCount";
        final String PAGE = "page";
        modelAndView.addObject(SEARCH_CRITERIA,new SearchCriteria());
        modelAndView.addObject(NEWS_TO_DELETE,new DeleteNewsTO());
        modelAndView.addObject(NEWS_TO,news);
        modelAndView.addObject(AUTHOR_LIST,authors);
        modelAndView.addObject(TAG_LIST,tags);
        modelAndView.addObject(PAGE_COUNT, newsService.countOfPage(newsCount));
        modelAndView.addObject(PAGE,pageNumber);
        modelAndView.setViewName(viewName);
    }

}
