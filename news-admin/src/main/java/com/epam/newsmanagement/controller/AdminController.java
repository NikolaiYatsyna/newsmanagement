package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {

    @RequestMapping(value = {"/","/login"},method = RequestMethod.GET)
    public ModelAndView login() {
        return new ModelAndView("loginPage");
    }

    @RequestMapping(value = {"/403",},method = RequestMethod.GET)
    public ModelAndView accessDenied() {
        return new ModelAndView("error-403");
    }

    @RequestMapping(value = {"/404",},method = RequestMethod.GET)
    public ModelAndView notFound() {
        return new ModelAndView("error-404");
    }

}
