package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.to.DeleteCommentTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.util.RedirectAttributesBuilder;
import com.epam.newsmanagement.validator.СommentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private СommentValidator commentValidator;

    private static final String REDIRECT_PREFIX = "redirect:";
    private static final String NEWS_MAPPING ="/news/" ;

    private static final String ADD_COMMENT_MAPPING = "/postcomment";
    private static final String DELETE_COMMENT_MAPPING = "/deletecomments/{id}";

    private static final String COMMENT_MODEL_ATTRIBUTE = "comment";
    private static final String COMMENT_DELETE_MODEL_ATTRIBUTE = "deleteCommentTO";
    private static final String NEWS_ID = "id";

    @InitBinder(COMMENT_MODEL_ATTRIBUTE)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(commentValidator);
    }

    @RequestMapping(value = ADD_COMMENT_MAPPING, method = RequestMethod.POST)
    public ModelAndView addComment(@ModelAttribute(COMMENT_MODEL_ATTRIBUTE) @Validated Comment comment, BindingResult bindingResult,
                                   final RedirectAttributes redirectAttributes ) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        final String MESSAGE = "Comment added successfully";
        if(bindingResult.hasErrors()){
            RedirectAttributesBuilder.buildErrorAttributes(redirectAttributes,bindingResult,COMMENT_MODEL_ATTRIBUTE);
        }else{
            commentService.create(comment);
            RedirectAttributesBuilder.buildSuccessAttributes(redirectAttributes,MESSAGE);
        }
        redirectAttributes.addFlashAttribute(COMMENT_MODEL_ATTRIBUTE,comment);
        String REDIRECT_PATH = REDIRECT_PREFIX + NEWS_MAPPING + comment.getNews().getNewsId();
        modelAndView.setViewName(REDIRECT_PATH);
        return modelAndView;
    }

    @RequestMapping(value = DELETE_COMMENT_MAPPING,method = RequestMethod.POST)
    public ModelAndView deleteComment(@ModelAttribute(COMMENT_DELETE_MODEL_ATTRIBUTE) DeleteCommentTO deleteCommentTO,@PathVariable(NEWS_ID) Long id, final RedirectAttributes redirectAttributes) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        commentService.deleteAll(deleteCommentTO);
        String REDIRECT_PATH = REDIRECT_PREFIX + NEWS_MAPPING + id;
        modelAndView.setViewName(REDIRECT_PATH);
        return modelAndView;
    }
}
