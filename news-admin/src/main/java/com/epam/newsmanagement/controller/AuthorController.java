package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.util.RedirectAttributesBuilder;
import com.epam.newsmanagement.validator.AuthorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static com.epam.newsmanagement.util.RedirectAttributesBuilder.buildErrorAttributes;

@Controller
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorValidator authorValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(authorValidator);
    }

    private static final String EDIT_AUTHOR_MAPPING = "/editauthor";
    private static final String UPDATE_AUTHOR_MAPPING = "/updateauthor" ;
    private static final String EDIT_AUTHOR_VIEW = "editauthor";
    private static final String ADD_AUTHOR_MAPPING = "/addauthor";
    private static final String EXPIRE_AUTHOR_MAPPING = "/expiredAuthor";
    private static final String AUTHOR_EDIT_MODEL_ATTRIBUTE = "author";
    private static final String AUTHOR_ADD_MODEL_ATTRIBUTE = "addAuthor";


    private static final String REDIRECT_PREFIX = "redirect:";

    @RequestMapping(value = EDIT_AUTHOR_MAPPING, method = RequestMethod.GET)
    public String editAuthorPage(Model model) throws ServiceException {
        return buildDefaultPage(model);
    }

    @RequestMapping(value = ADD_AUTHOR_MAPPING, method = RequestMethod.GET)
    public String addAuthorPage(Model model) throws ServiceException {
        return buildDefaultPage(model);
    }

    @RequestMapping(value = ADD_AUTHOR_MAPPING, method = RequestMethod.POST)
    public ModelAndView addAuthor(@ModelAttribute(AUTHOR_ADD_MODEL_ATTRIBUTE) @Validated Author author, BindingResult bindingResult,
                                  final RedirectAttributes redirectAttributes) throws ServiceException {
        final String SUCCESS_MESSAGE = "Author added successfully";
        final String ADD_FLAG = "add";
        if(bindingResult.hasErrors()){
            buildErrorAttributes(redirectAttributes,bindingResult, AUTHOR_ADD_MODEL_ATTRIBUTE);
        }else {
            Long id = authorService.create(author);
            author.setAuthorId(id);
            RedirectAttributesBuilder.buildSuccessAttributes(redirectAttributes,SUCCESS_MESSAGE);
        }
        redirectAttributes.addFlashAttribute(ADD_FLAG,true);
        redirectAttributes.addFlashAttribute(AUTHOR_ADD_MODEL_ATTRIBUTE,author);
        ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX+EDIT_AUTHOR_MAPPING);
        return modelAndView;
    }

    @RequestMapping(value = EXPIRE_AUTHOR_MAPPING, method = RequestMethod.POST)
    public ModelAndView expireAuthor(@ModelAttribute(AUTHOR_EDIT_MODEL_ATTRIBUTE) @Validated Author author, BindingResult bindingResult,
                                  final RedirectAttributes redirectAttributes) throws ServiceException {
        final String SUCCESS_MESSAGE = "Author expired successfully";
        if(bindingResult.hasErrors()){
            buildErrorAttributes(redirectAttributes,bindingResult, AUTHOR_EDIT_MODEL_ATTRIBUTE);
        }else {
            author.setExpired(new Timestamp(new Date().getTime()));
            authorService.update(author);
            RedirectAttributesBuilder.buildSuccessAttributes(redirectAttributes,SUCCESS_MESSAGE);
        }
        redirectAttributes.addFlashAttribute(AUTHOR_EDIT_MODEL_ATTRIBUTE,author);
        ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX+EDIT_AUTHOR_MAPPING);
        return modelAndView;
    }

    @RequestMapping(value = UPDATE_AUTHOR_MAPPING,method = RequestMethod.POST)
    public ModelAndView editAuthor(@ModelAttribute(AUTHOR_EDIT_MODEL_ATTRIBUTE) @Validated  Author author, BindingResult bindingResult,
                                final RedirectAttributes redirectAttributes) throws ServiceException {
        final String SUCCESS_MESSAGE = "Tag updated successfully";
        if(bindingResult.hasErrors()){
            buildErrorAttributes(redirectAttributes,bindingResult, AUTHOR_EDIT_MODEL_ATTRIBUTE);
        }else {
            authorService.update(author);
            RedirectAttributesBuilder.buildSuccessAttributes(redirectAttributes,SUCCESS_MESSAGE);
        }
        redirectAttributes.addFlashAttribute(AUTHOR_EDIT_MODEL_ATTRIBUTE,author);
        ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + EDIT_AUTHOR_MAPPING);
        return modelAndView;
    }

    private String buildDefaultPage(Model model) throws ServiceException {
        final String AUTHORS_LIST = "authors";
        List<Author> authors = authorService.findAll();
        model.addAttribute(AUTHORS_LIST,authors);
        if(!model.containsAttribute(AUTHOR_EDIT_MODEL_ATTRIBUTE)) {
            model.addAttribute(AUTHOR_EDIT_MODEL_ATTRIBUTE, new Author());
        }
        if(!model.containsAttribute(AUTHOR_ADD_MODEL_ATTRIBUTE)) {
            model.addAttribute(AUTHOR_ADD_MODEL_ATTRIBUTE, new Author());
        }
        return EDIT_AUTHOR_VIEW;
    }


}
