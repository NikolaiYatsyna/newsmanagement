package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.RedirectAttributesBuilder;
import com.epam.newsmanagement.validator.TagValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class TagController {

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TagValidator tagValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(tagValidator);
    }

    private static final String EDIT_TAG_MAPPING = "/edittags" ;
    private static final String UPDATE_TAG_MAPPING = "/updatetag" ;
    private static final String ADD_TAG_MAPPING = "/addtag" ;
    private static final String DELETE_TAG_MAPPING = "/deletetag/{id}" ;

    private static final String TAG_EDIT_MODEL_ATTRIBUTE = "tag";
    private static final String TAG_ADD_MODEL_ATTRIBUTE = "addTag";
    private static final String TAGS = "tags";
    private static final String EDIT_TAG_VIEW = "edittags";
    private static final String REDIRECT_PREFIX = "redirect:";

    private static final String ID_VARIABLE = "id";

    @RequestMapping(value = EDIT_TAG_MAPPING )
    public String editTagPage(Model model) throws ServiceException {
        return buildDefaultPage(model);
    }

    @RequestMapping(value = ADD_TAG_MAPPING,method = RequestMethod.GET)
    public String addTagRequest(Model model) throws ServiceException {
        return buildDefaultPage(model);
    }

    @RequestMapping(value = ADD_TAG_MAPPING,method = RequestMethod.POST)
    public ModelAndView addTag(@ModelAttribute(TAG_ADD_MODEL_ATTRIBUTE) @Validated Tag tag,BindingResult bindingResult,
                               final RedirectAttributes redirectAttributes) throws ServiceException {
        final String SUCCESS_MESSAGE = "Tag added successfully";
        final String ADD_FLAG = "add";
        if(bindingResult.hasErrors()){
            RedirectAttributesBuilder.buildErrorAttributes(redirectAttributes,bindingResult,TAG_ADD_MODEL_ATTRIBUTE);
        }else {
            tagService.create(tag);
            RedirectAttributesBuilder.buildSuccessAttributes(redirectAttributes,SUCCESS_MESSAGE);
        }
        redirectAttributes.addFlashAttribute(ADD_FLAG,true);
        redirectAttributes.addFlashAttribute(TAG_ADD_MODEL_ATTRIBUTE,tag);
        ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX+EDIT_TAG_MAPPING);
        return modelAndView;
    }

    @RequestMapping(value = UPDATE_TAG_MAPPING, method = RequestMethod.POST)
    public ModelAndView editTag(@ModelAttribute(TAG_EDIT_MODEL_ATTRIBUTE) @Validated  Tag tag, BindingResult bindingResult,
                                final RedirectAttributes redirectAttributes) throws ServiceException {
        final String SUCCESS_MESSAGE = "Tag updated successfully";
        if(bindingResult.hasErrors()){
            RedirectAttributesBuilder.buildErrorAttributes(redirectAttributes,bindingResult,TAG_EDIT_MODEL_ATTRIBUTE);
        }else {
            tagService.update(tag);
            RedirectAttributesBuilder.buildSuccessAttributes(redirectAttributes,SUCCESS_MESSAGE);
        }
        redirectAttributes.addFlashAttribute(TAG_EDIT_MODEL_ATTRIBUTE,tag);
        ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + EDIT_TAG_MAPPING);
        return modelAndView;
    }

    @RequestMapping(value = DELETE_TAG_MAPPING, method = RequestMethod.POST)
    public ModelAndView deleteTag(@PathVariable(ID_VARIABLE) Long id)throws ServiceException {
        tagService.delete(id);
        ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + EDIT_TAG_MAPPING);
        return modelAndView;
    }

    private String buildDefaultPage(Model model) throws ServiceException {
        List<Tag> tags = tagService.findAll();
        model.addAttribute(TAGS,tags);
        if(!model.containsAttribute(TAG_EDIT_MODEL_ATTRIBUTE)) {
            model.addAttribute(TAG_EDIT_MODEL_ATTRIBUTE, new Tag());
        }
        if(!model.containsAttribute(TAG_ADD_MODEL_ATTRIBUTE)) {
            model.addAttribute(TAG_ADD_MODEL_ATTRIBUTE, new Tag());
        }
        return EDIT_TAG_VIEW;
    }

}
