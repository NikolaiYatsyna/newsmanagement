package com.epam.newsmanagement.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ListIterator;

public class WelcomeFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter (ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.sendRedirect(servletRequest.getServletContext().getContextPath() + "/FrontController?commandName=viewAllNews");
    }

    @Override
    public void destroy() {

    }
}
