package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.controller.command.CommandException;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Date;

@Component
public class PostComment implements Command {

    @Autowired
    CommentService commentService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String PATH = "/FrontController?commandName=viewNews&newsId=";
        Comment comment = createComment(request);
        try {
            commentService.create(comment);
        } catch (ServiceException e) {
           throw new CommandException(e);
        }
        request.setAttribute(ACTION, REDIRECT);
        return PATH+comment.getNews().getNewsId();
    }

    private Comment createComment(HttpServletRequest request){
        final String NEWS_ID_PARAM = "newsId";
        final String TEXT_PARAM = "text";
        Long newsId = Long.valueOf(request.getParameter(NEWS_ID_PARAM));
        String text = request.getParameter(TEXT_PARAM);
        Comment comment = new Comment();
        News news = new News();
        news.setNewsId(newsId);
        comment.setNews(news);
        comment.setText(text);
        comment.setCreationDate(new Timestamp(new Date().getTime()));
        return comment;
    }
}
