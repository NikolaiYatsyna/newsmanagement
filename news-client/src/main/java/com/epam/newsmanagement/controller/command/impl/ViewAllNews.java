package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.controller.command.CommandException;
import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ComplexNewsService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class ViewAllNews implements Command {

    @Autowired
    AuthorService authorService;
    @Autowired
    NewsService newsService;
    @Autowired
    ComplexNewsService complexNewsService;
    @Autowired
    TagService tagService;

    private static final Long DEFAULT_PAGE_NUMBER = 1L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        final String PAGE_ATTRIBUTE = "page";
        final String TARGET = "/allNews";
        final SearchCriteria criteria = new SearchCriteria();
        Long pageNumber;
        List<NewsTO> newsTOList;
        List<Author> authors;
        List<Tag> tags;
        Long pageCount;

        try {
            pageNumber = Long.parseLong(request.getParameter(PAGE_ATTRIBUTE));
        }catch (NumberFormatException e){
            pageNumber = DEFAULT_PAGE_NUMBER;
        }

        try {
            newsTOList = complexNewsService.findNewsTOPage(pageNumber);
            authors = authorService.findAll();
            tags = tagService.findAll();
            pageCount = PaginationUtil.calculatePagesCount(newsService.countOfNodes());
        }catch (ServiceException e){
            throw new CommandException(e);
        }

        fillRequestAttributes(request,authors,tags,newsTOList,pageCount,criteria,pageNumber);
        return TARGET;
    }

    private void fillRequestAttributes(HttpServletRequest request, List<Author> authors, List<Tag> tags, List<NewsTO> newsTOList,
                                       Long pageCount, SearchCriteria criteria, Long pageNumber){
        request.setAttribute("authorList", authors);
        request.setAttribute("tagList", tags);
        request.setAttribute("newsTO", newsTOList);
        request.setAttribute("pageCount", pageCount);
        HttpSession session = request.getSession();
        session.setAttribute("searchCriteria", criteria);
        session.removeAttribute("currentPage");
        session.setAttribute("currentPage", pageNumber);
        request.setAttribute(ACTION,FORWARD);
    }
}
