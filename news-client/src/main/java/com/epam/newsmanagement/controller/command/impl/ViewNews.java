package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.controller.command.CommandException;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ComplexNewsService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class ViewNews implements Command {

    @Autowired
    AuthorService authorService;
    @Autowired
    NewsService newsService;
    @Autowired
    ComplexNewsService complexNewsService;
    @Autowired
    TagService tagService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        final String VIEW_NEWS = "/viewNews";
        Long previousId;
        Long nextId;
        try {
            String newsIdParam = request.getParameter("newsId");

            Long newsId = Long.parseLong(newsIdParam);
            NewsTO newsTO = complexNewsService.findNewsTO(newsId);


            previousId = complexNewsService.findPrevious(newsId);
            nextId = complexNewsService.findNext(newsId);

            fillRequest(request,newsTO,previousId,nextId);

           return VIEW_NEWS;

        } catch (ServiceException | NumberFormatException  e) {
            throw new CommandException(e);
        }
    }

    private void fillRequest(HttpServletRequest request,NewsTO newsTO,Long previousId,Long nextId){
        request.setAttribute("newsTO", newsTO);
        request.setAttribute("previousId", previousId);
        request.setAttribute("nextId", nextId);
        request.setAttribute(ACTION, FORWARD);
    }
}
