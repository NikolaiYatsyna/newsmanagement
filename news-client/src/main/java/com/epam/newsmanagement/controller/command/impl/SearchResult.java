package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.controller.command.CommandException;
import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ComplexNewsService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
public class SearchResult implements Command{

    @Autowired
    ComplexNewsService complexNewsService;
    @Autowired
    NewsService newsService;
    @Autowired
    AuthorService authorService;
    @Autowired
    TagService tagService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        final String TAG_PARAM = "tagId";
        final String AUTHOR_PARAM = "authorId";
        final String CRITERIA ="searchCriteria";
        final String PAGE_PARAM = "currentPage";
        final String TARGET = "/allNews";
        final HttpSession session = request.getSession();
        SearchCriteria searchCriteria;
        Long page = (Long) session.getAttribute(PAGE_PARAM);
        String[] tagParameters = request.getParameterValues(TAG_PARAM);
        String[] authorParameters = request.getParameterValues(AUTHOR_PARAM);

        if(tagParameters==null & authorParameters == null){
            searchCriteria = (SearchCriteria) session.getAttribute(CRITERIA);
            if(searchCriteria==null){
                final String path = "/error";
                request.setAttribute(ACTION,REDIRECT);
                return path;
            }
        }else{
            session.removeAttribute(CRITERIA);
            List<Long> tagsId = createTagList(tagParameters);
            List<Long> authorsId = createAuthorList(authorParameters);
            searchCriteria = new SearchCriteria();
            searchCriteria.setAuthorId(authorsId);
            searchCriteria.setTagsId(tagsId);

            session.setAttribute(CRITERIA,searchCriteria);
            request.setAttribute(ACTION,FORWARD);
        }

        List<Author> authors;
        List<Tag> tags;
        Long pageCount;
        List<NewsTO> newsTOList;
        try {
            newsTOList = complexNewsService.findNewsTOPageByCriteria(searchCriteria,page);
            authors = authorService.findAll();
            tags = tagService.findAll();
            pageCount = PaginationUtil.calculatePagesCount(newsService.countOfNodesByCriteria(searchCriteria));
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        fillRequestAttributes(request,authors,tags,newsTOList,pageCount,searchCriteria,page);
        return TARGET;
    }

    private List<Long> createTagList(String[] tagParameters){
        List<Long> tagList = new ArrayList<>();

        for(String tag : tagParameters){
            tagList.add(Long.parseLong(tag));
        }
        return tagList;
    }

    private List<Long> createAuthorList(String[] authorParameters){
        List<Long> authors = new ArrayList<>();
        for(String author : authorParameters){
            authors.add(Long.parseLong(author));
        }
        return authors;
    }

    private void fillRequestAttributes(HttpServletRequest request, List<Author> authors, List<Tag> tags, List<NewsTO> newsTOList,
                                       Long pageCount, SearchCriteria criteria, Long pageNumber){
        request.setAttribute("authorList", authors);
        request.setAttribute("tagList", tags);
        request.setAttribute("newsTO", newsTOList);
        request.setAttribute("pageCount", pageCount);
        HttpSession session = request.getSession();
        session.setAttribute("searchCriteria", criteria);
        session.removeAttribute("currentPage");
        session.setAttribute("currentPage", pageNumber);
        request.setAttribute(ACTION,FORWARD);
    }
}
