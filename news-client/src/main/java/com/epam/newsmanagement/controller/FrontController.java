package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.controller.command.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



public class FrontController extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(FrontController.class);

    private WebApplicationContext context;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        ServletContext servletContext = this.getServletContext();
        context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String NAME_PARAM  = "commandName";
        final String DEFAULT_NAME = "viewAllNews";
        String name = req.getParameter(NAME_PARAM);
        if(name==null){
            name = DEFAULT_NAME;
        }
        Command command = (Command) context.getBean(name);
        String path;

        if(command!=null){
            try {
                path = command.execute(req,resp);
            } catch (CommandException e) {
                LOGGER.error("Unexpected exception",e);
                req.setAttribute(Command.ACTION,Command.REDIRECT);
                path = "/error";
            }
        }else{
            req.setAttribute(Command.ACTION,Command.REDIRECT);
            path = "/error";
        }

        String action = (String) req.getAttribute(Command.ACTION);
        switch (action){
            case Command.FORWARD:
                req.getRequestDispatcher(path).forward(req,resp);
                break;
            case Command.REDIRECT:
                resp.sendRedirect(getServletContext().getContextPath()+path);
        }
    }
}
