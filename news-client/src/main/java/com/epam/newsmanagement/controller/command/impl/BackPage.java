package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.controller.command.CommandException;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class BackPage implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        final String PAGE_NUMBER =  "pageNumber";
        final String CRITERIA = "searchCriteria";
        final String CRITERIA_EMPTY = "/FrontController?commandName=viewAllNews&page=";
        final String CRITERIA_NOT_NULL = "/FrontController?commandName=searchResult&page=";
        final Long DEFAULT_PAGE = 1L;
        final HttpSession session = request.getSession();

        Long page = (Long) session.getAttribute(PAGE_NUMBER);
        if(page==null){
            page = DEFAULT_PAGE;
        }
        String path;
        SearchCriteria criteria = (SearchCriteria) session.getAttribute(CRITERIA);

        if (criteria != null) {
            if(criteria.getAuthorId() == null){
                path = CRITERIA_EMPTY + page;
            }else{
                path = CRITERIA_NOT_NULL + page;
            }
        }else{
            path = CRITERIA_EMPTY + page;
        }

        request.setAttribute(ACTION,REDIRECT);

        return path;
    }
}
