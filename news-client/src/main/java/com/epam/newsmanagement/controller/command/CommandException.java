package com.epam.newsmanagement.controller.command;

public class CommandException extends Exception {
    public CommandException(Throwable cause) {
        super(cause);
    }
}
