package com.epam.newsmanagement.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {
    String ACTION = "action";
    String FORWARD = "forward";
    String REDIRECT = "redirect";

    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
