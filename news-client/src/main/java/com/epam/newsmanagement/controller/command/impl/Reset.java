package com.epam.newsmanagement.controller.command.impl;

import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.controller.command.CommandException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class Reset implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        final String TARGET = "/FrontController?commandName=viewAllNews";
        final String CRITERIA = "searchCriteria";
        HttpSession session = request.getSession();
        session.removeAttribute(CRITERIA);
        request.setAttribute(ACTION, REDIRECT);
        return TARGET;
    }
}
