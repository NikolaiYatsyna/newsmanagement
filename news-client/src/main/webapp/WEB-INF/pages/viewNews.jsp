<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <Title>View News</Title>

    <c:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>

    <c:url value="/resources/css/style.css" var="userStyle"/>
    <link href="${userStyle}" rel="stylesheet"/>

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">News Portal</a>
        </div>
    </div>
</nav>
<div class="row col-md-15 content page-wrap">

    <div class="row col-md-10 col-md-offset-1" style="padding-left: 50px">
        <div>
            <div class="row col-md-16">
                <div class="row">
                    <p class="text-left">${newsTO.news.title}</p>
                </div>
                <div class="row col-md-14">
                    <div class="row">

                        <c:forEach items="${newsTO.authorList}" var="author">
                            <div class="col-md-2"><p class="glyphicon glyphicon-user"> ${author.name}</p></div>
                        </c:forEach>
                        <div class="col-md-6 pull-right">
                            <p class="text-right glyphicon glyphicon-calendar"> Creation date : <fmt:formatDate dateStyle="short" value="${newsTO.news.creationDate}"/></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p class="glyphicon glyphicon-tags">
                        <c:forEach var="tag" items="${newsTO.tagList}">
                            <span>${tag.name};</span>
                        </c:forEach>
                    </p>
                </div>
                <div class="row col-md-14">
                    <p>${newsTO.news.fullText}</p>
                </div>
                    <c:if test="${not empty newsTO.commentList}"> <div class="row col-md-14">
                        <p class="text-right"><div class="text">Comments:</div></p>
                        <c:forEach var="item" items="${newsTO.commentList}">
                            <div class="row col-md-14">
                                <div class="col-md-12" style="padding-bottom:10px" >
                                    <p class="text">
                                    <div class="text-left" >
                                        <fmt:formatDate dateStyle="short" value="${item.creationDate}"/>
                                    </div>
                                    <div class="text-left">${item.text}</div>
                                </div>
                                </p>
                            </div>
                        </c:forEach>
                    </div></c:if>

                <div class="row">
                    <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/FrontController?commandName=postComment">
                        <input type = "hidden" value="${newsTO.news.newsId}" name="newsId"/>
                        <div class="row col-md-2"><label>Post comment:</label></div>
                        <input type="text-area" cssClass="form-control" style="margin: 0px; height: 30px; width: 950px;" name="text"/>
                        <input type="submit" class="btn btn-success" value="Post comment">
                    </form>
                    <div class="row col-md-14">
                        <ul class="pager">
                            <li class="previous">
                                <a href="${pageContext.request.contextPath}/FrontController?commandName=viewNews&newsId=${previousId}">&larr; Previous</a>
                            </li>
                            <li class="btn">
                                <a href="${pageContext.request.contextPath}/FrontController?commandName=backPage">Back</a>
                            </li>
                            <li class="next">
                                <a href="${pageContext.request.contextPath}/FrontController?commandName=viewNews&newsId=${nextId}">Next &rarr;</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<footer class="footer" id="footer">
    <div class="col-md-15">
        <p class = "text-muted text-center footer-text ">Mikalai Yatsyna. EPAM 2016</p>
    </div>
</footer>
</body>


</html>