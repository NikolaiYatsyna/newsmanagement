<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <Title>View All News</Title>

    <c:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>

    <c:url value="/resources/css/style.css" var="userStyle"/>
    <link href="${userStyle}" rel="stylesheet"/>

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/FrontController?commandName=viewAllNews">News Portal</a>
        </div>
    </div>
</nav>
<div class="row col-md-15 col-md-offset-1 page-wrap">

    <div class="row col-md-11">
        <div class="row ">
            <div class="row col-md-offset-1">
                        <form class="form" method="POST"
                                 action="${pageContext.request.contextPath}/FrontController?commandName=searchResult">
                            <div class="col-md-5">
                                <select class="form-control" multiple="multiple" name="authorId">
                                    <option disabled value>Select author</option>
                                    <c:forEach items="${authorList}" var="author">
                                        <option value="${author.getAuthorId() }">${author.getName()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" multiple="multiple" name="tagId">
                                    <option disabled value>Select tags</option>
                                    <c:forEach items="${tagList}" var="tag">
                                        <option value="${tag.getTagId() }">${tag.getName()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <input class="btn btn-info" type="submit" value="Filter" />
                            <input class="btn btn-info" type="submit" value="Reset" formaction="${pageContext.request.contextPath}/FrontController?commandName=reset"/>
                        </form>
            </div>
            <div class="col-md-14">
                <c:forEach var="item" items="${newsTO}">
                    <div class="row col-md-11 news">
                        <div class="col-md-14 col-md-offset-1">
                            <div class="row">
                                <a href="${pageContext.request.contextPath}/FrontController?commandName=viewNews&newsId=${item.news.newsId}"><p class="text-left">${item.news.title}</p></a>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <c:forEach items="${item.authorList}" var="author">
                                        <div class="col-md-2"><p class="glyphicon glyphicon-user"> ${author.name}</p></div>
                                    </c:forEach>
                                    <div class="col-md-6 col-md-offset-4">
                                        <p class="text-right glyphicon glyphicon-calendar"> Creation date : <fmt:formatDate dateStyle="short" value="${item.news.creationDate}"/></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <p class="glyphicon glyphicon-tags">
                                    <c:forEach var="tag" items="${item.tagList}">
                                        <span> ${tag.name};</span>
                                    </c:forEach>
                                </p>
                            </div>
                            <div class="row col-md-14">
                                <p>${item.news.shortText}</p>
                            </div>

                            <div class="row col-md-14">
                                <div><p class="glyphicon glyphicon-comment">  ${fn:length(item.commentList)} Comments</p></div>

                            </div>
                            <div class="row">
                                <a href="${pageContext.request.contextPath}/FrontController?commandName=viewNews&newsId=${item.news.newsId}" class="btn btn-default pull-right">More details</a>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
            <div class="col-md-6 col-md-offset-2 pagination-centered">
                    <ul id="pagination-demo" class="pagination">
                        <li><a href="${pageContext.request.contextPath}/FrontController?commandName=viewAllNews&page/1">&laquo;</a></li>
                        <input type="hidden" value="${pageCount}" id="pageCount">
                        <c:forEach var="pageNumber" end="35" begin="1">
                            <li><a href="${pageContext.request.contextPath}/FrontController?commandName=viewAllNews&page=${pageNumber}">${pageNumber}</a></li>
                        </c:forEach>
                        <li><a href="${pageContext.request.contextPath}/FrontController?commandName=viewAllNews&page=${pageCount}">&raquo;</a></li>
                    </ul>
            </div>
        </div>

    </div>

</div>
<footer class="footer" id="footer">
    <div class="col-md-15">
        <p class = "text-muted text-center footer-text ">Mikalai Yatsyna. EPAM 2016</p>
    </div>
</footer>
</body>
<c:url value="/resources/js/jquery-2.1.3.min.js" var="jquery"/>
<script type='text/javascript' src="${jquery}"></script>
<c:url value="/resources/js/pagination.js" var="pagination"/>
<script type='text/javascript' src="${pagination}" ></script>
<c:url value="/resources/js/jquery.twbsPagination.min.js" var="jQueryPagination"/>
<script type='text/javascript' src="${jQueryPagination}" ></script>
</html>