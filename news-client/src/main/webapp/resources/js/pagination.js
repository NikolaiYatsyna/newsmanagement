$(function(){
    var  pages = $('input#pageCount').val();
    $('#pagination-demo').twbsPagination({
        totalPages: pages,
        visiblePages: 3,
        href: '?viewAllNews&page={{number}}',
        onPageClick: function (event, page) {
            $('#page-content').text('Page ' + page);
        }
    });
});