<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Unexpected Error</title>
    <c:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <c:url value="/resources/css/style.css" var="style"/>
    <link href="${style}" rel="stylesheet"/>
</head>
<body>
<div class="error-container">
    <div class="error-container row-fluid">
        <div class="col-lg-12">
            <div class="centering text-center error-container">
                <div class="text-center">
                    <h2 class="without-margin">Don't worry. It's <span class="text-warning">
                    <big></big></span> error only.</h2>
                    <h4 class="text-warning">Unexpected error while processing request.Try again later.</h4>
                </div>
                <div class="text-center">
                    <h3><small>Choose an option below</small></h3>
                </div>
                <hr>
                <ul class="pager">
                    <li><a href="${pageContext.request.contextPath}/">All news</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
    <footer class="footer">
        <div class="col-md-15">
            <p class = "text-muted text-center footer-text ">Mikalai Yatsyna. EPAM 2016</p>
        </div>
    </footer>
</body>
</html>
