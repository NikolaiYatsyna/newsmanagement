package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.bean.Author;

import java.util.List;
/**
 * Describes specific operation which can be performed with {@link Author} objects in the database
 */
public interface AuthorDAO extends GenericDAO<Author> {
    /**
     * Loads non-expired authors from the database.
     * @return list of non-expired authors from database.
     */
    List<Author> loadNonExpiredAuthors();

    /**
     * Loads {@link Author} object from the database by name.
     * @param name name of author.
     * @return {@link Author} object with required name.
     */
    Author loadByName(String name);

    /**
     * Loads {@link Author} object from the database by newsId.
     * @param newsId id of news.
     * @return {@link Author} object with requied newsId.
     */
    List<Author> loadByNewsId(Long newsId);
}
