package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.bean.User;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class UserDAOImpl implements UserDAO {

    private static final String JPQL_LOAD_ALL = "Select user From User user";
    private static final String JPQL_LOAD_BY_ID = "Select user From User user where user.userId =:userId";
    private static final String JPQL_LOAD_BY_LOGIN = "Select user from User user where user.login =:login";
    private static final String JPQL_DELETE = "delete from User user where user.userId =:userId ";


    @PersistenceContext
    EntityManager manager;

    @Override
    public User loadByLogin(String login){
        Query query = manager.createQuery(JPQL_LOAD_BY_LOGIN);
        query.setParameter("login",login);
        User user = (User) query.getResultList().get(0);
        return user;
    }

    @Override
    public List<User> loadAll(){
        Query query = manager.createQuery(JPQL_LOAD_ALL);
        List<User> users = query.getResultList();
        return users;
    }

    @Override
    public User loadOneById(Long id){
        Query query = manager.createQuery(JPQL_LOAD_BY_ID);
        query.setParameter("userId",id);
        User user = (User) query.getResultList().get(0);
        return user;
    }

    @Override
    public Long create(User user){
        manager.persist(user);
        manager.flush();
        return user.getUserId();
    }

    @Override
    public void delete(User user){
        Query query = manager.createQuery(JPQL_DELETE);
        query.setParameter("userId",user.getUserId());
        query.executeUpdate();
    }

    @Override
    public void update(User user){
        manager.merge(user);
        manager.flush();
    }
}
