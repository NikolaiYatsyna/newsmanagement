package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.bean.Role;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class RoleDAOImpl implements RoleDAO{
    private static final String JPQL_LOAD_ALL = "select role from Role role ";
    private static final String JPQL_LOAD_BY_ID = "select role from Role role where role.roleId=:roleId";
    private static final String JPQL_DELETE= "delete from Role role where role.roleId=:roleId";

    @PersistenceContext
    EntityManager manager;

    @Override
    public List<Role> loadAll(){
        Query query = manager.createQuery(JPQL_LOAD_ALL);
        List<Role> roles = query.getResultList();
        return roles;
    }

    @Override
    public Role loadOneById(Long id){
        Query query = manager.createQuery(JPQL_LOAD_BY_ID);
        query.setParameter("roleId",id);
        Role role = (Role) query.getResultList().get(0);
        return role;
    }

    @Override
    public Long create(Role role){
        manager.persist(role);
        manager.flush();
        return role.getRoleId();
    }

    @Override
    public void delete(Role role){
        Query query = manager.createQuery(JPQL_DELETE);
        query.setParameter("roleId",  role.getRoleId());
        query.executeUpdate();
    }

    @Override
    public void update(Role role){
        manager.merge(role);
        manager.flush();
    }
}
