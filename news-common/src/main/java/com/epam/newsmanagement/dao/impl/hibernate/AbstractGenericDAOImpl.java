package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.GenericDAO;
import org.hibernate.Session;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import java.util.List;

public abstract class AbstractGenericDAOImpl<T>  extends HibernateDaoSupport implements GenericDAO<T> {

    private Class type;

    public AbstractGenericDAOImpl(Class<T> type) {
        this.type = type;
    }

    @Override
    public List<T> loadAll() {
        List<T> list = null;
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.setCacheQueries(true);
        return (List<T>) hibernateTemplate.loadAll(type);
    }

    @Override
    public T loadOneById(Long id){
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        T entity = (T) hibernateTemplate.get(type,id);
        return entity;
    }

    @Override
    public Long create(T t){
        Session session = getSessionFactory().getCurrentSession();
        Long id = (Long)    session.save(t);
        session.flush();
        return id;
    }

    @Override
    public void delete(T entity){
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.delete(entity);
    }

    @Override
    public void update(T t){
        Session session = getSessionFactory().getCurrentSession();
        session.saveOrUpdate(t);
        session.flush();
    }
}
