package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.bean.Role;

/**
 * Describes specific operation which can be performed with {@link Role} objects in the database.
 */
public interface RoleDAO extends GenericDAO<Role>{
}
