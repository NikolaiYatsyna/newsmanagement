package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.bean.Tag;
import org.springframework.orm.hibernate5.HibernateTemplate;

import java.util.List;

public class TagDAOImpl extends AbstractGenericDAOImpl<Tag> implements TagDAO {

    private static final String HQL_SELECT_TAG_BY_NEWS_ID =  "SELECT tag FROM News news JOIN news.tags tag WHERE news.id = ? ";

    public TagDAOImpl(Class type) {
        super(type);
    }

    @Override
    public List<Tag> loadByNewsId(Long newsId) {
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.setCacheQueries(true);
        Long[] queryParams = {newsId};
        return (List<Tag>) hibernateTemplate.find(HQL_SELECT_TAG_BY_NEWS_ID, queryParams);
    }
}
