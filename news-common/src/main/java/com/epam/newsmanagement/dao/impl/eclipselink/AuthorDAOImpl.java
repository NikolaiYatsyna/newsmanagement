package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.bean.Author;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class AuthorDAOImpl implements AuthorDAO {

    private static final String JPQL_DELETE = "Delete From Author author where author.authorId =:authorId";
    private static final String JPQL_LOAD_BY_ID = "Select author from Author author where author.authorId=:authorId";
    private static final String JPQL_LOAD_ALL = "select author from Author author";
    private static final String JPQL_LOAD_BY_NEWS_ID = "select author from Author author join author.news as news where news.newsId=:newsId";
    private static final String JPQL_LOAD_BY_NAME = "select author from Author author where author.name=:authorName";
    private static final String JPQL_LOAD_NON_EXPIRED = "select author from Author author where author.expired=null";

    @PersistenceContext
    EntityManager manager;

    @Override
    public List<Author> loadNonExpiredAuthors() {

        Query query = manager.createQuery(JPQL_LOAD_NON_EXPIRED);
        List<Author> authors = query.getResultList();
        return authors;
    }

    @Override
    public Author loadByName(String name) {
        Query query = manager.createQuery(JPQL_LOAD_BY_NAME);
        query.setParameter("authorName", name);
        Author author = (Author) query.getResultList().get(0);
        return author;
    }

    @Override
    public List<Author> loadByNewsId(Long newsId){
        Query query = manager.createQuery(JPQL_LOAD_BY_NEWS_ID);
        query.setParameter("newsId",newsId);
        List<Author> authors = query.getResultList();
        return authors;
    }

    @Override
    public List<Author> loadAll(){
        Query query = manager.createQuery(JPQL_LOAD_ALL);
        List<Author> authors = query.getResultList();
        return authors;
    }

    @Override
    public Author loadOneById(Long id){

        Query query = manager.createQuery(JPQL_LOAD_BY_ID);
        query.setParameter("authorId",id);
        Author author = (Author) query.getResultList().get(0);
        return author;
    }

    @Override
    public Long create(Author author){
        manager.persist(author);
        manager.flush();
        return author.getAuthorId();
    }

    @Override
    public void delete(Author author){
        Query query = manager.createQuery(JPQL_DELETE);
        query.setParameter("authorId",author.getAuthorId());
        query.executeUpdate();
    }

    @Override
    public void update(Author author){
        manager.merge(author);
        manager.flush();
    }
}
