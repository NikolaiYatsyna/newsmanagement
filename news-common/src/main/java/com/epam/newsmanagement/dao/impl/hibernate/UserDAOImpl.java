package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.bean.User;
import org.springframework.orm.hibernate5.HibernateTemplate;

import java.util.List;

public class UserDAOImpl extends AbstractGenericDAOImpl<User> implements UserDAO {

    private static final String HQL_LOAD_BY_LOGIN = "SELECT user FROM User user WHERE user.login = ? ";

    public UserDAOImpl(Class<User> type) {
        super(type);
    }

    @Override
    public User loadByLogin(String login) {

        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.setCacheQueries(true);
        String[] queryParams = {login};
        List<User> users = (List<User>) hibernateTemplate.find(HQL_LOAD_BY_LOGIN, queryParams);
        return users.get(0);

    }
}
