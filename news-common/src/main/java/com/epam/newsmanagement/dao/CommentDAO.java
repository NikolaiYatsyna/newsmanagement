package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.bean.Comment;

import java.util.List;
/**
 * Describes specific operation which can be performed with {@link Comment} objects in the database
 */
public interface CommentDAO extends GenericDAO<Comment> {
    /**
     * Loads list of {@link Comment} object from the database by newsId.
     * @param newsId id number of news.
     * @return list of {@link Comment} object with required newsId.
     **/
    List<Comment> loadByNewsId(Long newsId);

    /**
     * Deletes {@link Comment} by newsId.
     * @param newsId id number of news.
     */
    void deleteByNewsId(Long newsId);

    /**
     * Deletes {@link Comment} objects specified in list.
     * @param comments list of comments need to be deleted.
     */
    void deleteAll(List<Comment> comments);
}
