package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.bean.Author;
import org.springframework.orm.hibernate5.HibernateTemplate;

import java.util.List;


public class AuthorDAOImpl extends AbstractGenericDAOImpl<Author> implements AuthorDAO {
    private static final String HQL_LOAD_NON_EXPIRED = "Select author From Author author Where author.expired is null";
    private static final String HQL_LOAD_BY_NAME =  "Select author FROM Author author Where author.name = ? ";
    private static final String HQL_LOAD_BY_NEWS_ID =  "Select author FROM News news Join news.authors author Where news.id = ? ";

    public AuthorDAOImpl(Class<Author> type) {
        super(type);
    }

    @Override
    public List<Author> loadNonExpiredAuthors() {
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.setCacheQueries(true);
        return (List<Author>) hibernateTemplate.find(HQL_LOAD_NON_EXPIRED, null);
    }

    @Override
    public Author loadByName(String name){
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.setCacheQueries(true);
        String[] queryParams = {name};
        List<Author> authors = (List<Author>) hibernateTemplate.find(HQL_LOAD_BY_NAME,queryParams);
        return authors.get(0);
    }

    @Override
    public List<Author> loadByNewsId(Long newsId){
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.setCacheQueries(true);
        Long[] queryParams = {newsId};
        return (List<Author>) hibernateTemplate.find(HQL_LOAD_BY_NEWS_ID,queryParams);
    }
}
