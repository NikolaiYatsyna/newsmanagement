package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.bean.User;

/**
 * Describes specific operation which can be performed with {@link User} objects in the database.
 */
public interface UserDAO extends GenericDAO<User>{
    /**
     * Loads {@link User} object from the database by login.
     * @param login login of {@link User} object which is need to be loaded from the database.
     * @return {@link User} object with with required login.
     */
    User loadByLogin(String login);
}
