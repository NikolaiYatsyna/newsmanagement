package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.bean.Comment;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class CommentDAOImpl implements CommentDAO{

    private static final String JPQL_DELETE = "Delete From Comment comment where comment.commentId =:commentId";
    private static final String JPQL_LOAD_BY_ID  = "Select comment from Comment comment where comment.commentId=:commentId";
    private static final String JPQL_LOAD_ALL = "select comment from Comment comment";
    private static final String JPQL_LOAD_BY_NEWS_ID = "select comment from Comment comment join comment.news as news where news.newsId=:newsId";
    private static final String SQL_DELETE_BY_NEWS_ID = "Delete from COMMENTS where CMT_NWS_ID = ?";

    @PersistenceContext
    EntityManager manager;

    @Override
    public List<Comment> loadByNewsId(Long newsId){
        Query query = manager.createQuery(JPQL_LOAD_BY_NEWS_ID);
        query.setParameter("newsId",newsId);
        List<Comment> comments = query.getResultList();
        return comments;
    }

    @Override
    public void deleteByNewsId(Long newsId){
        Query query = manager.createNativeQuery(SQL_DELETE_BY_NEWS_ID);
        query.setParameter(1,newsId);
        query.executeUpdate();
    }

    @Override
    public void deleteAll(List<Comment> comments){
        Query query = manager.createQuery(JPQL_DELETE);
        for(Comment comment : comments){
            query.setParameter("commentId",comment.getCommentId());
            query.executeUpdate();
        }
    }

    @Override
    public List<Comment> loadAll(){
        Query query = manager.createQuery(JPQL_LOAD_ALL);
        List<Comment> comments = query.getResultList();
        return comments;
    }

    @Override
    public Comment loadOneById(Long id){
        Query query = manager.createQuery(JPQL_LOAD_BY_ID);
        query.setParameter("commentId",id);
        Comment comment = (Comment) query.getResultList().get(0);
        return comment;
    }

    @Override
    public Long create(Comment comment){
        manager.persist(comment);
        manager.flush();
        return comment.getCommentId();
    }

    @Override
    public void delete(Comment comment){
        Query query = manager.createQuery(JPQL_DELETE);
        query.setParameter("commentId",comment.getCommentId());
        query.executeUpdate();
    }

    @Override
    public void update(Comment comment){
        manager.merge(comment);
        manager.flush();
    }


}
