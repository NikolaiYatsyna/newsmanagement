package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;

import java.util.List;
/**
 * Describes specific operation which can be performed with {@link News} objects in database
 */
public interface NewsDAO extends GenericDAO<News> {

    /**
     * Load {@link News} objects from the database with grouped by number of news from start param to end param.
     * @param start first index of news.
     * @param end last index of news.
     * @return List of {@link News} objects from start index to end index.
     */

    List<News> loadNewsPage(Long start,Long end);

    /**
     * Load {@link News} objects from the database with grouped by number of news from start param to end param and {@link SearchCriteria} object.
     * @param start first index of news.
     * @param end last index of news.
     * @return List of {@link News} objects from start index to end index by {@link SearchCriteria} object..
     */
    List<News> loadNewsPageByCriteria(SearchCriteria criteria,Long start,Long end);

    /**
     * Load {@link News} object from the database by {@link SearchCriteria} object.
     * @param criteria {@link SearchCriteria} object which contains parameters for search.
     * @return list of news with parameters satisfying the requirements described in {@link SearchCriteria} object.
     */
    List<News> loadByCriteria(SearchCriteria criteria);

    /**
     * Counts number of {@link News} objects in the database.
     * @return number of {@link News} objects in the database.
     */
    Long countNews();

    /**
     * Deletes {@link News} objects with id specified in params.
     * @param news of {@link News} object id which need to be deleted.
     */
    void delete(List<News> news);


    Long findNext(Long id);

    Long findPrevious(Long id);

    Long findNextByCriteria(Long id,SearchCriteria criteria);

    Long findPreviousByCriteria(Long id,SearchCriteria criteria);
}
