package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.bean.News;
import org.hibernate.CacheMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;

import java.util.List;

public class CommentDAOImpl extends AbstractGenericDAOImpl<Comment> implements CommentDAO {

    private static final String HQL_LOAD_BY_NEWS = "select comment from News news join news.comments as comment " +
                                                                            "where news.newsId=:newsId";

    public CommentDAOImpl(Class<Comment> type) {
        super(type);
    }

    @Override
    public List<Comment> loadByNewsId(Long newsId){
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.setCacheMode(CacheMode.GET);
        Query query = session.createQuery(HQL_LOAD_BY_NEWS);
        query.setParameter("newsId", newsId);
        return query.list();
    }

    @Override
    public void deleteByNewsId(Long newsId) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Comment comment = new Comment();
        News news = new News();
        comment.setNews(news);
        session.delete(comment);
        session.flush();
    }

    @Override
    public void deleteAll(List<Comment> comments) {
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        hibernateTemplate.deleteAll(comments);
    }
}
