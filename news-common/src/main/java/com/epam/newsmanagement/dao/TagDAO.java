package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.bean.Tag;

import java.util.List;
/**
 * Describes specific operation which can be performed with {@link Tag} objects in the database.
 */
public interface TagDAO extends GenericDAO<Tag> {
    /**
     * Loads {@link Tag} object from the database by news newsId.
     * @param newsId id number of news.
     * @return {@link Tag} object with required newsId.
     */
    List<Tag> loadByNewsId(Long newsId);
}
