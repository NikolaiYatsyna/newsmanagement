package com.epam.newsmanagement.dao.impl.eclipselink;


import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.util.QueryBuilder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;

@Component
public class NewsDAOImpl implements NewsDAO {

    private static final String JPQL_LOAD_ALL = "Select news From News news left join news.comments as comments group by news.newsId,news.title,news.shortText,news.fullText," +
            "news.creationDate,news.modificationDate,news.version order by count(comments) desc ,news.modificationDate desc ";
    private static final String JPQL_LOAD_BY_ID = "select news From News news where news.newsId=:newsId";
    private static final String JPQL_DELETE = "delete from News news where news.newsId=:newsId";

    private static final String SQL_FIND_PAGE = "SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date\n" +
            "FROM (SELECT T.*,ROWNUM RN FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
            "AS count FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.cmt_nws_id = nws_id\n"  +
            "GROUP BY nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date,nws_modification_date\n"+
            "ORDER BY count DESC, nws_modification_date DESC) T) WHERE RN >= ? AND RN <= ?";

    private static final String SQL_FIND_PAGE_PART1 = "SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date\n" +
            "FROM (SELECT T.*,ROWNUM RN FROM(\n"+
            "SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
            "AS count FROM (";
    private static final String SQL_FIND_PAGE_PART2 = ") JOIN COMMENTS ON COMMENTS.cmt_nws_id = nws_id\n" +
            "GROUP BY nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date,nws_modification_date\n" +
            "ORDER BY count DESC, nws_modification_date DESC) T) WHERE RN >= ? AND RN <=?";

    private static final String SQL_NEWS_COUNT = "SELECT COUNT(NWS_ID) FROM NEWS";

    private static final String SQL_NEXT_QUERY = "Select next_news from (SELECT nws_id,LEAD(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as next_news\n" +
            " FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
            "  AS count FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.cmt_nws_id = nws_id  GROUP BY nws_id, nws_title, nws_short_text,nws_full_text, nws_creation_date,nws_modification_date\n" +
            "  ORDER BY count DESC, nws_modification_date DESC)) WHERE nws_id = ?";

    private static final String  SQL_PREVIOUS_QUERY = "Select prev_news from (SELECT nws_id,Lag(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as prev_news\n" +
            " FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
            "  AS count FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.cmt_nws_id = nws_id  GROUP BY nws_id, nws_title, nws_short_text,nws_full_text, nws_creation_date,nws_modification_date\n" +
            "  ORDER BY count DESC, nws_modification_date DESC)) WHERE nws_id = ?";

    private static final String SQL_PREVIOUS_WITH_CRITERIA_1 = "Select prev_news from (SELECT nws_id,LAG(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as prev_news\n" +
            "                       FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
            "                      AS count FROM (";
    private static final String SQL_PREVIOUS_WITH_CRITERIA_2 = " ) JOIN COMMENTS ON COMMENTS.cmt_nws_id = NWS_ID GROUP BY NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE,NWS_MODIFICATION_DATE )) WHERE nws_id = ?";

    private static final String SQL_NEXT_WITH_CRITERIA_1 = "Select next_news from (SELECT nws_id,LEAD(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as next_news\n" +
            "                       FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
            "                      AS count FROM (";
    private static final String SQL_NEXT_WITH_CRITERIA_2 = " ) JOIN COMMENTS ON COMMENTS.cmt_nws_id = NWS_ID GROUP BY NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE,NWS_MODIFICATION_DATE )) WHERE nws_id = ?";



    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<News> loadNewsPage(Long start, Long end){
        EntityManager manager = getEntityManager();
        Query nativeQuery = manager.createNativeQuery(SQL_FIND_PAGE,News.class);
        nativeQuery.setParameter(1,start);
        nativeQuery.setParameter(2,end);
        List<News> news = (List<News>) nativeQuery.getResultList();
        return news;
    }

    @Override
    public List<News> loadNewsPageByCriteria(SearchCriteria criteria, Long start, Long end){
        EntityManager manager = getEntityManager();
        String queryString = new StringBuilder().append(SQL_FIND_PAGE_PART1).append(QueryBuilder.build(criteria)).append(SQL_FIND_PAGE_PART2).toString();
        Query nativeQuery = manager.createNativeQuery(queryString,News.class);
        nativeQuery.setParameter(1,start);
        nativeQuery.setParameter(2,end);
        List<News> news = nativeQuery.getResultList();
        return news;
    }

    @Override
    public List<News> loadByCriteria(SearchCriteria criteria){
        EntityManager manager = getEntityManager();
        String queryString = QueryBuilder.build(criteria);
        Query nativeQuery = manager.createNativeQuery(queryString,News.class);
        List<News> news = nativeQuery.getResultList();
        return news;
    }

    @Override
    public Long countNews(){
        EntityManager manager = getEntityManager();
        Query nativeQuery = manager.createNativeQuery(SQL_NEWS_COUNT);
        BigDecimal firstResult = (BigDecimal)nativeQuery.getResultList().get(0);
        return Long.valueOf(String.valueOf(firstResult));
    }

    @Override
    public Long findNext(Long id){
        EntityManager manager = getEntityManager();
        Query nativeQuery = manager.createNativeQuery(SQL_NEXT_QUERY);
        nativeQuery.setParameter(1,id);
        BigDecimal firstResult = (BigDecimal) nativeQuery.getResultList().get(0);
        return firstResult==null ? id : Long.valueOf(String.valueOf(firstResult));
    }

    @Override
    public Long findPrevious(Long id){
        EntityManager manager = getEntityManager();
        Query nativeQuery = manager.createNativeQuery(SQL_PREVIOUS_QUERY);
        nativeQuery.setParameter(1,id);
        BigDecimal firstResult = (BigDecimal) nativeQuery.getResultList().get(0);
        return firstResult==null ? id :Long.valueOf(String.valueOf(firstResult));
    }

    @Override
    public Long findNextByCriteria(Long id, SearchCriteria criteria){
        EntityManager manager = getEntityManager();
        String query = new StringBuilder().append(SQL_NEXT_WITH_CRITERIA_1).append(QueryBuilder.build(criteria)).append(SQL_NEXT_WITH_CRITERIA_2).toString();
        Query nativeQuery = manager.createNativeQuery(query);
        nativeQuery.setParameter(1,id);
        BigDecimal firstResult = (BigDecimal) nativeQuery.getResultList().get(0);
        return firstResult==null ? id : Long.valueOf(String.valueOf(firstResult));
    }

    @Override
    public Long findPreviousByCriteria(Long id, SearchCriteria criteria){
        EntityManager manager = getEntityManager();
        String query = new StringBuilder().append(SQL_PREVIOUS_WITH_CRITERIA_1).append(QueryBuilder.build(criteria)).append(SQL_PREVIOUS_WITH_CRITERIA_2).toString();
        Query nativeQuery = manager.createNativeQuery(query);
        nativeQuery.setParameter(1,id);
        BigDecimal firstResult = (BigDecimal) nativeQuery.getResultList().get(0);
        return firstResult==null ? id : Long.valueOf(String.valueOf(firstResult));
    }

    @Override
    public void delete(List<News> news){
        EntityManager manager = getEntityManager();
        for(News newsItem : news){
            Query query = manager.createQuery(JPQL_DELETE);
            query.setParameter("newsId",newsItem.getNewsId());
            query.executeUpdate();
        }
    }

    @Override
    public List<News> loadAll(){
        EntityManager manager = getEntityManager();
        Query query = manager.createQuery(JPQL_LOAD_ALL);
        List<News> news = query.getResultList();
        return news;
    }

    @Override
    public News loadOneById(Long id){
        EntityManager manager = getEntityManager();
        Query query = manager.createQuery(JPQL_LOAD_BY_ID);
        query.setParameter("newsId",id);
        News news = (News) query.getResultList().get(0);
        return news;
    }

    @Override
    public Long create(News news){
        EntityManager manager = getEntityManager();
        manager.persist(news);
        manager.flush();
        return news.getNewsId();
    }

    @Override
    public void delete(News news){
        EntityManager manager = getEntityManager();
        Query query = manager.createQuery(JPQL_DELETE);
        query.setParameter("newsId",news.getNewsId());
        query.executeUpdate();
        manager.flush();
    }

    @Override
    public void update(News news){
        EntityManager manager = getEntityManager();
        manager.merge(news);
        manager.flush();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

}
