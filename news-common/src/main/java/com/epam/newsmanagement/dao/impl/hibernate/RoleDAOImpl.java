package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.bean.Role;

public class RoleDAOImpl extends AbstractGenericDAOImpl<Role> implements RoleDAO {
    public RoleDAOImpl(Class<Role> type) {
        super(type);
    }
}
