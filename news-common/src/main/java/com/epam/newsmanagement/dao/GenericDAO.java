package com.epam.newsmanagement.dao;

import java.util.List;

/**
 * Basic interface which describes common operations with entity objects in the database.
 * @param <Entity> generic type of objects which serve as entity.
 */
public interface GenericDAO<Entity> {
    /**
     * Load all {@link Entity} objects  from the database.
     * @return List of existing {@link Entity} objects.
     */
    List<Entity> loadAll();
    /**
     * Load {@link Entity} object with required id from the database.
     * @param id value of primary key of {@link Entity} object in database.
     * @return {@link Entity} object with required id.
     */
    Entity loadOneById(Long id);

    /**
     * Create a record in database which contains all values from input object.
     * @param entity an {@link Entity} object which need to be inserted into the database.
     * @return id number of created {@link Entity}
     */
    Long create(Entity entity);

    /**
     * Deletes {@link Entity} object with id specified in params.
     * @param entity of {@link Entity} object id which need to be deleted.
     */
    void delete(Entity entity);

    /**
     * Updates record in database with values specified in input {@link Entity} object.
     * @param entity {@link Entity} object which is need to be updated in database.
     */
    void update(Entity entity);
}
