package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.bean.Tag;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class TagDAOImpl implements TagDAO {

    private static final String JPQL_LOAD_BY_NEWS_ID = "select tag from Tag tag join tag.news as news where news.newsId=:newsId";
    private static final String JPQL_LOAD_ALL = "select tag from Tag tag ";
    private static final String JPQL_LOAD_BY_ID = "select tag from Tag tag where tag.tagId=:tagId";
    private static final String JPQL_DELETE= "delete from Tag tag where tag.tagId=:tagId";

    @PersistenceContext
    EntityManager manager;

    @Override
    public List<Tag> loadByNewsId(Long newsId){
        Query query = manager.createQuery(JPQL_LOAD_BY_NEWS_ID);
        query.setParameter("newsId",newsId);
        List<Tag> resultList = (List<Tag>) query.getResultList();
        return resultList;
    }

    @Override
    public List<Tag> loadAll(){
        Query query = manager.createQuery(JPQL_LOAD_ALL);
        List<Tag> tags = query.getResultList();
        return tags;
    }

    @Override
    public Tag loadOneById(Long id){
        Query query = manager.createQuery(JPQL_LOAD_BY_ID);
        query.setParameter("tagId",id);
        Tag tag = (Tag) query.getResultList().get(0);
        return tag;
    }

    @Override
    public Long create(Tag tag){
        manager.persist(tag);
        manager.flush();
        return tag.getTagId();
    }

    @Override
    public void delete(Tag tag){
        Query query = manager.createQuery(JPQL_DELETE);
        query.setParameter("tagId", tag.getTagId());
        query.executeUpdate();
    }

    @Override
    public void update(Tag tag) {
        manager.merge(tag);
        manager.flush();
    }
}
