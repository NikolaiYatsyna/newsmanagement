package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.util.QueryBuilder;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate5.HibernateTemplate;

import java.math.BigDecimal;
import java.util.List;


public class NewsDAOImpl extends AbstractGenericDAOImpl<News> implements NewsDAO {

    private static final String NEXT_QUERY = "Select next_news from (SELECT nws_id,LEAD(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as next_news\n" +
        "FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id) AS count FROM NEWS FULL OUTER JOIN COMMENTS " +
        "ON COMMENTS.cmt_nws_id = nws_id  GROUP BY nws_id, nws_title, nws_short_text,nws_full_text, nws_creation_date,nws_modification_date ORDER BY count DESC, " +
        "nws_modification_date DESC)) WHERE nws_id = ?";

    private static final String PREVIOUS_QUERY = "Select prev_news from (SELECT nws_id,Lag(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as prev_news\n" +
        " FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id) AS count FROM NEWS FULL OUTER JOIN COMMENTS " +
        "ON COMMENTS.cmt_nws_id = nws_id  GROUP BY nws_id, nws_title, nws_short_text,nws_full_text, nws_creation_date,nws_modification_date ORDER BY count DESC," +
        "nws_modification_date DESC)) WHERE nws_id =?";

    private static final String HQL_LOAD_ALL = "Select news From News news left join news.comments as comments group by news.id,news.title,news.shortText,news.fullText," +
            "news.creationDate,news.modificationDate order by count(comments) desc ,news.modificationDate desc ";

    public NewsDAOImpl(Class type) {
        super(type);
    }


    @Override
    public List<News> loadNewsPage(Long start, Long end) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(HQL_LOAD_ALL);
        query.setFirstResult(start.intValue() - 1);
        query.setMaxResults((int) (end - start));
        return query.setCacheable(true).list();
    }


    @Override
    public List<News> loadNewsPageByCriteria(SearchCriteria criteria, Long start, Long end) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        String query = QueryBuilder.build(criteria);
        SQLQuery sqlQuery = session.createSQLQuery(query).addEntity(News.class);
        sqlQuery.setFirstResult(start.intValue() - 1);
        sqlQuery.setMaxResults(end.intValue() - start.intValue() - 1);
        return sqlQuery.setCacheable(true).list();
    }

    @Override
    public Long findNext(Long id) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Query sqlQuery = session.createSQLQuery(NEXT_QUERY);
        sqlQuery.setParameter(0, id);
        BigDecimal firstResult = (BigDecimal) sqlQuery.list().get(0);
        return firstResult == null ? id : Long.valueOf(String.valueOf(firstResult));
    }

    @Override
    public Long findPrevious(Long id) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        SQLQuery sqlQuery = session.createSQLQuery(PREVIOUS_QUERY);
        sqlQuery.setParameter(0, id);
        BigDecimal firstResult = (BigDecimal) sqlQuery.list().get(0);
        return firstResult == null ? id : Long.valueOf(String.valueOf(firstResult));
    }


    @Override
    public List<News> loadByCriteria(SearchCriteria searchCriteria) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Query sqlQuery = session.createSQLQuery(QueryBuilder.build(searchCriteria)).addEntity(News.class);
        return sqlQuery.setCacheable(true).list();
    }

    @Override
    public Long findNextByCriteria(Long id, SearchCriteria criteria) {
        final String NEXT_WITH_CRITERIA_1 = "Select next_news from (SELECT nws_id,LEAD(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as next_news\n" +
                "FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
                "AS count FROM (";
        final String NEXT_WITH_CRITERIA_2 = " ) JOIN COMMENTS ON COMMENTS.cmt_nws_id = NWS_ID GROUP BY NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE," +
                "NWS_MODIFICATION_DATE )) WHERE nws_id = ?";
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        String query = new StringBuilder()
                .append(NEXT_WITH_CRITERIA_1)
                .append(QueryBuilder.build(criteria))
                .append(NEXT_WITH_CRITERIA_2)
                .toString();
        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter(0, id);
        BigDecimal firstResult = (BigDecimal) sqlQuery.list().get(0);
        return firstResult == null ? id : Long.valueOf(String.valueOf(firstResult));
    }

    @Override
    public Long findPreviousByCriteria(Long id, SearchCriteria criteria) {
        final String PREVIOUS_WITH_CRITERIA_1 = "Select prev_news from (SELECT nws_id,LAG(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as prev_news\n" +
                "FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id)\n" +
                "AS count FROM (";
        final String PREVIOUS_WITH_CRITERIA_2 = " ) JOIN COMMENTS ON COMMENTS.cmt_nws_id = NWS_ID GROUP BY NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE," +
                "NWS_MODIFICATION_DATE )) WHERE nws_id = ?";
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        String query = new StringBuilder()
                .append(PREVIOUS_WITH_CRITERIA_1)
                .append(QueryBuilder.build(criteria))
                .append(PREVIOUS_WITH_CRITERIA_2)
                .toString();
        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter(0, id);
        BigDecimal firstResult = (BigDecimal) sqlQuery.list().get(0);
        return firstResult == null ? id : Long.valueOf(String.valueOf(firstResult));
    }

    public Long countNews() {
        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        DetachedCriteria criteria = DetachedCriteria.forClass(News.class);
        criteria.setProjection(Projections.rowCount());
        List<?> byCriteria = hibernateTemplate.findByCriteria(criteria);
        return (Long) byCriteria.get(0);
    }

    @Override
    public void delete(List<News> news) {
        SessionFactory sessionFactory = getSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        for (News aNew : news) {
            session.delete(aNew);
            session.flush();
        }
    }
}
