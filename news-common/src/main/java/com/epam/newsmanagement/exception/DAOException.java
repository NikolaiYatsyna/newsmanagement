package com.epam.newsmanagement.exception;

/**
 * Simple wrapper for {@link java.sql.SQLException}
 */
public class DAOException extends RuntimeException {
    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
