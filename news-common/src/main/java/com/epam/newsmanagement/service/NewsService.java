package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;

import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Decribes operations which can be perform with {@link News} object.
 */
public interface NewsService extends GenericService<News> {

    /**
     * Loads page of news with specified size.
     * @param page number of page.
     * @return List of news with specified size.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<News> findPage(Long page) throws ServiceException;
    /**
     * Loads page of news with specified size by criteria.
     * @param page number of page.
     * @return List of news with specified size  by criteria.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<News> findPageByCriteria(SearchCriteria searchCriteria, Long page) throws ServiceException;

    /**
     * Finds all news corresponding to searchCriteria.
     * @param searchCriteria object with search parameters.
     * @return list of news corresponding to searchCriteria.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<News> findByCriteria(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Counts number of news.
     * @return number of news.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    Long countOfNodes() throws ServiceException;

    /**
     * Returns number of pages with news.
     * @param newsCount number of news.
     * @return number of pages.
     */
    Long countOfPage(Long newsCount);

    Long countOfNodesByCriteria(SearchCriteria criteria) throws ServiceException;

    /**
     * Deletes {@link News} object by id.
     * @param id id of  {@link News} object which is need to be deleted.
     * @throws ServiceException if exception in DAO layer was caught.
     */
    void delete(Long... id) throws ServiceException;

    Long findNext(Long id) throws ServiceException;

    Long findPrevious(Long id) throws ServiceException;

    Long findNextByCriteria(Long id, SearchCriteria criteria) throws ServiceException;

    Long findPreviousByCriteria(Long id, SearchCriteria criteria) throws ServiceException;
}
