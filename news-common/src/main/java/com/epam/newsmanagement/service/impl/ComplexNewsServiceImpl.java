
package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.to.DeleteNewsTO;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementation of {@link ComplexNewsService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class ComplexNewsServiceImpl implements ComplexNewsService {

    @Autowired
    private  NewsService newsService;
    @Autowired
    private  AuthorService authorService;
    @Autowired
    private  CommentService commentService;
    @Autowired
    private  TagService tagService;


    /**
     * Implementation of {@link ComplexNewsService#createNewsWithTagsAndAuthor(News, List, List)} method.
     */
    @Override
    public Long createNewsWithTagsAndAuthor(News news, List<Long> authors, List<Long> tags) throws ServiceException {

        Timestamp creationDate = new Timestamp(new Date().getTime());
        java.sql.Date modificationDate = new java.sql.Date(creationDate.getTime());
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);
        Long newsId = newsService.create(news);
        return newsId;
    }

    @Override
    public void updateNews(News news, List<Long> authorsIdList, List<Long> tagsIdList) throws ServiceException {
        java.sql.Date modificationDate = new java.sql.Date(new Date().getTime());
        news.setModificationDate(modificationDate);
        newsService.update(news);
    }

    /**
     * Implementation of {@link ComplexNewsService#deleteNewsWithTagsAndAuthor(DeleteNewsTO)} method.
     */
    @Override
    public void deleteNewsWithTagsAndAuthor(DeleteNewsTO deleteNewsTO) throws ServiceException {
        List<Long> deleteNewsList = deleteNewsTO.getDeleteNewsList();
        Long[] deleteNewsId = deleteNewsList.toArray(new Long[deleteNewsList.size()]);
        commentService.deleteByNewsId(deleteNewsId);
        newsService.delete(deleteNewsId);
    }

    /**
     * Implementation of {@link ComplexNewsService#findNewsTO(Long)} method.
     */
    @Override
    public NewsTO findNewsTO(Long id) throws ServiceException{
        return findOneNewsTO(id);
    }

    /**
     * Implementation of {@link ComplexNewsService#findAllNewsTO()} method.
     */
    @Override
    public List<NewsTO> findAllNewsTO() throws ServiceException {
       List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.findAll();
        for(News news : newsList){
            NewsTO newsTO = findNewsTO(news.getNewsId());
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }
    public List<NewsTO> findNewsTOPage(Long page) throws ServiceException {
        List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.findPage(page);
        for(News news : newsList){
            NewsTO newsTO = findNewsTO(news.getNewsId());
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }

    @Override
    public List<NewsTO> findNewsTOPageByCriteria(SearchCriteria searchCriteria, Long page) throws ServiceException {
        List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.findPageByCriteria(searchCriteria, page);
        for(News news : newsList){
            NewsTO newsTO = findNewsTO(news.getNewsId());
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }

    private NewsTO findOneNewsTO(Long id) throws ServiceException{
        NewsTO newsTO = new NewsTO();
        News news = newsService.findById(id);
        List<Tag> tags = tagService.findByNewsId(id);
        List<Comment> comments = commentService.findByNewsId(id);
        List<Author> author= authorService.findByNewsId(id);
        newsTO.setNews(news);
        newsTO.setAuthorList(author);
        newsTO.setTagList(tags);
        newsTO.setCommentList(comments);
        return newsTO;
    }

    @Override
    public List<NewsTO> findBySearchCriteria(SearchCriteria criteria) throws ServiceException{
        List<News> newsList = newsService.findByCriteria(criteria);
        List<NewsTO> newsTOList = new ArrayList<>();
        for(News news : newsList){
            newsTOList.add(findNewsTO(news.getNewsId()));
        }
        return newsTOList;
    }

    @Override
    public Long findNext(Long id) throws ServiceException {
        Long nextId =  newsService.findNext(id);
        return nextId;
    }

    @Override
    public Long findPrevious(Long id) throws ServiceException {
        Long previousId = newsService.findPrevious(id);
        return previousId;
    }

    @Override
    public Long findNextByCriteria(Long id, SearchCriteria criteria) throws ServiceException {
        Long nextId =  newsService.findNextByCriteria(id, criteria);
        return nextId;
    }

    @Override
    public Long findPreviousByCriteria(Long id, SearchCriteria criteria) throws ServiceException {
        Long previousId = newsService.findPreviousByCriteria(id,  criteria);
        return previousId;
    }
}
