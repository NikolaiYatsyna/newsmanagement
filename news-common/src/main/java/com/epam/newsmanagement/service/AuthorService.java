package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.bean.Author;

import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;


/**
 * Describes operations which can be perform with {@link Author} object.
 */
public interface AuthorService extends GenericService<Author> {
    /**
     * Loads all non expired authors(they'r expired date is bigger than current date(time).
     * @return list of non-epired authors.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<Author> findNonExpired() throws ServiceException;

    /**
     * Finds author in the database with required login.
     * @param name name of author.
     * @return {@link Author} object with required login.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    Author findByName(String name) throws ServiceException;
    /**
     * Finds author in database with required newsId.
     * @param newsId id of news.
     * @return Authors with required newsId.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List <Author> findByNewsId(Long newsId) throws ServiceException;
}
