package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.bean.User;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link UserService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    /**
     * Implementation of {@link UserService#findById(Long)} mehtod.
     */
    @Override
    public User findById(Long id) throws ServiceException {
        User user;
        try {
            user = userDAO.loadOneById(id);
        }
        catch (Exception e){
            LOGGER.error("Failed to load user by id ",e);
            throw new ServiceException(e);
        }
        return user;
    }


    /**
     * Implementation of {@link UserService#findByLogin(String)} method.
     */
    @Override
    public User findByLogin(String login) throws ServiceException{
        User user;
        try {
            user = userDAO.loadByLogin(login);
        }
        catch (Exception e){
            LOGGER.error("Failed to load user by login ",e);
            throw new ServiceException(e);
        }
        return user;
    }


    /**
     * Implementation of {@link UserService#update(Object)} method.
     */
    @Override
    public void update(User user) throws ServiceException{
        try {
            userDAO.update(user);
        }catch (Exception e){
            LOGGER.error("Failed to update user ",e);
            throw new ServiceException(e);
        }
    }


    /**
     * Implementation of {@link UserService#delete(Long)}method .
     */
    @Override
    public void delete(Long id) throws ServiceException{

        try {
            User user = new User();
            user.setUserId(id);
            userDAO.delete(user);
        } catch (Exception e) {
            LOGGER.error("Failed to delete user with id = "+id,e);
            throw new ServiceException(e);
        }
    }


    /**
     * Implementation of {@link UserService#create(Object)} method.
     */
    @Override
    public Long create(User user) throws ServiceException{
        Long id;
        try {
            id = userDAO.create(user);
        } catch (Exception e) {
            LOGGER.error("Failed to create user ",e);
            throw new ServiceException(e);
        }
        return id;
    }


    /**
     * Implementation of {@link UserService#findAll()} method.
     */
    @Override
    public List<User> findAll() throws ServiceException{
        List<User> users;
        try {
            users = userDAO.loadAll();
        }
        catch (Exception e){
            LOGGER.error("Failed to load user by login ",e);
            throw new ServiceException(e);
        }
        return users;
    }
}
