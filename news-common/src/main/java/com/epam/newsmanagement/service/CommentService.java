package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.to.DeleteCommentTO;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Decribes operations which can be perform with {@link Comment} object.
 */
public interface CommentService extends GenericService<Comment> {
    /**
     * Finds all comments in the database with required newsId.
     * @param newsId id of news.
     * @return List of comments with required newsId.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<Comment> findByNewsId(Long newsId) throws ServiceException;
    /**
     * Deletes all comments in the database with required newsId.
     * @param newsId id of news.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    void deleteByNewsId(Long[] newsId) throws ServiceException;

    /**
     * Deletes {@link Comment} objects specified in list.
     * * @param comments list of comments need to be deleted.
     * @throws ServiceException if there were errors while reading {@link Comment} objects from the database.
     */
    void deleteAll(DeleteCommentTO deleteCommentTO) throws ServiceException;
}
