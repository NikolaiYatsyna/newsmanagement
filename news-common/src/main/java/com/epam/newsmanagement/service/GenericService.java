package com.epam.newsmanagement.service;


import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Describes basic operation with {@link Entity} objects.
 * @param <Entity> generic type of objects which serve as entity.
 */
public interface GenericService<Entity> {
    /**
     * Creating an {@link Entity} object in the database.
     * @param entity entity which is need to be created in the database.
     * @return id of created in the database object.
     * @throws ServiceException if exception in DAO layer was caught.
     */
    Long create(Entity entity) throws ServiceException;

    /**
     * Finds all {@link Entity} objects.
     * @return list of {@link Entity} objects.
     * @throws ServiceException if exception in DAO layer was caught.
     */
    List<Entity> findAll() throws ServiceException;

    /**
     * Finds {@link Entity} object in the database by id.
     * @param id if of {@link Entity} object.
     * @return {@link Entity} object with required id.
     * @throws ServiceException if exception in DAO layer was caught.
     */
    Entity findById(Long id) throws ServiceException;

    /**
     * Deletes {@link Entity} object by id.
     * @param id id of  {@link Entity} object which is need to be deleted.
     * @throws ServiceException if exception in DAO layer was caught.
     */
    void delete(Long id) throws ServiceException;

    /**
     * Updates {@link Entity} object with actual field values.
     * @param entity {@link Entity} object which is need to be updated.
     * @throws ServiceException if exception in DAO layer was caught.
     */
    void update(Entity entity) throws ServiceException;
}
