package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Decribes operations which can be perform with {@link Tag} object.
 */
public interface TagService extends GenericService<Tag> {
    /**
     * Finds all tags in database with required newsId.
     * @param newsId id of news.
     * @return List of tags with required newsId.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<Tag> findByNewsId(Long newsId) throws ServiceException;
}
