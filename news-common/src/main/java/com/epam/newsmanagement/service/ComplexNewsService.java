package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.domain.to.DeleteNewsTO;
import com.epam.newsmanagement.domain.to.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Complex Service for operating with news,news authors,news comments,and news tags.
 */
public interface ComplexNewsService {
    /**
     * Loads page of news with specified size.
     * @param page number of page.
     * @return List of news with specified size.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<NewsTO> findNewsTOPage(Long page) throws ServiceException;
    /**
     * Loads page of news with specified size by criteria.
     * @param page number of page.
     * @return List of news with specified size  by criteria.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    List<NewsTO> findNewsTOPageByCriteria(SearchCriteria searchCriteria, Long page) throws ServiceException;
    /**
     * Creates news with author and tags.
     * @param news news object.
     * @param author news author object.
     * @param tags list of news tags.
     * @return id of news object.
     * @throws ServiceException if there were errors while performing operation.
     */
    Long createNewsWithTagsAndAuthor(News news, List<Long> author, List<Long> tags) throws ServiceException;

    void updateNews(News news,List<Long> authors,List<Long> tags) throws ServiceException;

    /**
     * Deletes news ,author,and tag wires.
     * @param deleteNewsTO special object with information for deletion.
     * @throws ServiceException if there were errors while performing operation.
     */
    void deleteNewsWithTagsAndAuthor(DeleteNewsTO deleteNewsTO) throws ServiceException;

    /**
     * Finds {@link NewsTO} object with required news id.
     * @param id news id.
     * @return {@link NewsTO} object with required news id.
     * @throws ServiceException if there were errors while performing operation.
     */
    NewsTO findNewsTO(Long id) throws ServiceException;

    /**
     * Finds all {@link NewsTO} objects.
     * @return list of {@link NewsTO} objects.
     * @throws ServiceException if there were errors while performing operation.
     */
    List<NewsTO> findAllNewsTO() throws ServiceException;

    /**
     * Finds all {@link NewsTO} objects corresponding to searchCriteria.
     * @param criteria object with search parameters.
     * @return list of newsTO corresponding to searchCriteria.
     * @throws ServiceException if there were errors while performing operation.
     */
    List<NewsTO> findBySearchCriteria(SearchCriteria criteria) throws ServiceException;

    /**
     * Loads next news with saving sorting order.
     * @param id id of current news.
     * @return next NewsTO object.
     * @throws ServiceException if there were errors while performing operation.
     */
    Long findNext(Long id) throws ServiceException;

    /**
     * * Loads previous news with saving sorting order.
     * @param id id of current news.
     * @return previous NewsTO object.
     * @throws ServiceException if there were errors while performing operation.
     */
    Long findPrevious(Long id) throws ServiceException;

    Long findNextByCriteria(Long id, SearchCriteria criteria) throws ServiceException;

    /**
     * * Loads previous news with saving sortring order.
     * @param id id of current news.
     * @return previous News object.
     * @throws ServiceException if there were errors while performing operation.
     */
    Long findPreviousByCriteria(Long id, SearchCriteria criteria) throws ServiceException;


}
