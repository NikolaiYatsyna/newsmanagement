package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.bean.Role;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.RoleService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link RoleService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class RoleServiceImpl implements RoleService {

    private static final Logger LOGGER = LogManager.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleDAO roleDAO;

    /**
     * Implementation of {@link RoleService#findById(Long)} method.
     */
    @Override
    public Role findById(Long id) throws ServiceException {
        Role role;
        try {
            role = roleDAO.loadOneById(id);
        }catch (Exception e){
            LOGGER.error("Failed to load Role with id = "+id,e);
            throw new ServiceException(e);
        }
        return role;
    }

}
