package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.util.PaginationUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Implementation of {@link NewsService} method.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements NewsService{

    @Autowired
    private  NewsDAO newsDAO;

    private static final Logger LOGGER = LogManager.getLogger(NewsServiceImpl.class);


    @Override
    public List<News> findPage(Long page) throws ServiceException{
        try {
            Long firstIndex = PaginationUtil.calculateFirstIndex(page);
            Long lastIndex = PaginationUtil.calculateLastIndex(page);
            return newsDAO.loadNewsPage(firstIndex,lastIndex);
        } catch (Exception e) {
            LOGGER.error("Failed to load all news ", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findPageByCriteria(SearchCriteria searchCriteria, Long page) throws ServiceException {
        try {
            Long firstIndex = PaginationUtil.calculateFirstIndex(page);
            Long lastIndex = PaginationUtil.calculateLastIndex(page);
            return newsDAO.loadNewsPageByCriteria(searchCriteria, firstIndex,lastIndex);
        } catch (Exception e) {
            LOGGER.error("Failed to load all news ", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News findById(Long newsId) throws ServiceException {
        try {
            return newsDAO.loadOneById(newsId);
        } catch (Exception e) {
            LOGGER.error("Failed to load News with id = " + newsId, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long newsId) throws ServiceException {
        try {
            News news = new News();
            news.setNewsId(newsId);
            newsDAO.delete(news);
        } catch (Exception e) {
            LOGGER.error("Failed to delete News with id = " + newsId, e);
            throw new ServiceException(e);
        }
    }


    @Override
    public void delete(Long... newsId) throws ServiceException {
        try {
            List<News> list = new ArrayList<>();
            for(Long id : newsId){
                News news = new News();
                news.setNewsId(id);
                list.add(news);
            }
            newsDAO.delete(list);
        } catch (Exception e) {
            LOGGER.error("Failed to delete News with id = " + newsId, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (Exception e) {
            LOGGER.error("Failed to update News: " + news, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findByCriteria(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.loadByCriteria(searchCriteria);
        } catch (Exception e) {
            LOGGER.error("Failed to load news by search criteria " + searchCriteria, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long countOfNodes() throws ServiceException {
        try {
            return newsDAO.countNews();
        } catch (Exception e) {
            LOGGER.error("Failed to count news ", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long create(News news) throws ServiceException {
        try {
            return newsDAO.create(news);
        } catch (Exception e) {
            LOGGER.error("Failed to create news :" + news, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findAll() throws ServiceException {
        try {
            return newsDAO.loadAll();
        } catch (Exception e) {
            LOGGER.error("Failed to load all news ", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long countOfPage(Long newsCount) {
        return PaginationUtil.calculatePagesCount(newsCount);
    }

    @Override
    public Long countOfNodesByCriteria(SearchCriteria criteria) throws ServiceException {
        return new Long(findByCriteria(criteria).size());
    }

    @Override
    public Long findNext(Long id) throws ServiceException {
        try {
            return newsDAO.findNext(id);
        } catch (Exception e) {
            LOGGER.error("Failed to load next news id",e);
            throw new ServiceException(e);
        }

    }

    @Override
    public Long findPrevious(Long id) throws ServiceException {
        try {
            return newsDAO.findPrevious(id);
        } catch (Exception e) {
            LOGGER.error("Failed to load previous news id",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long findNextByCriteria(Long id, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDAO.findNextByCriteria(id,criteria);
        } catch (Exception e) {
            LOGGER.error("Failed to load next news id",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long findPreviousByCriteria(Long id, SearchCriteria criteria) throws ServiceException {
        try {
            return newsDAO.findPreviousByCriteria(id,criteria);
        } catch (Exception e) {
            LOGGER.error("Failed to load previous news id",e);
            throw new ServiceException(e);
        }
    }
}
