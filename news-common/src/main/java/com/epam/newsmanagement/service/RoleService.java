package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.bean.Role;

import com.epam.newsmanagement.exception.ServiceException;

/**
 * Decribes operations which can be perform with {@link Role} object.
 */
public interface RoleService {
    /**
     * Finds role  in the database with required id.
     * @param id id of role.
     * @return List of comments with required newsId.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    Role findById(Long id)  throws ServiceException;
}
