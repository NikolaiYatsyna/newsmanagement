package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.to.DeleteCommentTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementation of {@link CommentService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDAO commentDAO;

    private static final Logger LOGGER = LogManager.getLogger(CommentServiceImpl.class);

    /**
     * Implementation of {@link CommentService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {

        try {
            Comment comment = new Comment();
            comment.setCommentId(id);
            commentDAO.delete(comment);
        } catch (Exception e) {
            LOGGER.error("Failed to delete comment with id = " + id, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#create(Object)} method.
     */
    @Override
    public Long create(Comment comment) throws ServiceException {
        try {
            comment.setCreationDate(new Timestamp((new Date()).getTime()));
            return commentDAO.create(comment);
        } catch (Exception e) {
            LOGGER.error("Failed to create comments :"+comment, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#findAll()} method.
     */
    @Override
    public List<Comment> findAll() throws ServiceException {
        try {
            return commentDAO.loadAll();
        } catch (Exception e) {
            LOGGER.error("Failed to find comments", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#update(Object)} method.
     */
    @Override
    public void update(Comment comment) throws ServiceException {
        try {
            commentDAO.update(comment);
        } catch (Exception e) {
            LOGGER.error("Failed to update comment:"+comment, e);
            throw new ServiceException(e);
        }

    }

    /**
     * Implementation of {@link CommentService#deleteByNewsId(Long[])} method.
     */
    @Override
    public void deleteByNewsId(Long[] newsId) throws ServiceException {
        try {
            for(Long id : newsId){
            commentDAO.deleteByNewsId(id);
            }
        } catch (Exception e) {
            LOGGER.error("Failed to delete comment with newsId"+newsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#findById(Long)} method.
     */
    @Override
    public Comment findById(Long id) throws ServiceException {
        try {
            return commentDAO.loadOneById(id);
        } catch (Exception e) {
            LOGGER.error("Failed to load comment with id = " + id, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#findByNewsId(Long)} method.
     */
    @Override
    public List<Comment> findByNewsId(Long newsId) throws ServiceException {
        try {
            return commentDAO.loadByNewsId(newsId);
        } catch (Exception e) {
            LOGGER.error("Failed to find comments with news  id = " +newsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#deleteAll(DeleteCommentTO)} )} method.
     */
    @Override
    public void deleteAll(DeleteCommentTO deleteCommentTO) throws ServiceException {
        try {
            List<Long> deleteCommentListId = deleteCommentTO.getDeleteCommentList();
            List<Comment> deleteCommentList = new ArrayList<>();
            for(Long id :deleteCommentListId){
                Comment comment = new Comment();
                comment.setCommentId(id);
                deleteCommentList.add(comment);
            }
            commentDAO.deleteAll(deleteCommentList);
        } catch (Exception e) {
            LOGGER.error("Failed to delete comment with newsId", e);
            throw new ServiceException(e);
        }
    }
}

