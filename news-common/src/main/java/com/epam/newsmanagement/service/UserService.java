package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.bean.User;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Decribes operations which can be perform with {@link User} object.
 */
public interface UserService extends GenericService<User> {
    /**
     * Finds user in the database with required login.
     * @param name login of user.
     * @return {@link User} object with required login.
     * @throws ServiceException if exception in DAO layer was thrown.
     */
    User findByLogin(String name) throws ServiceException;
}
