package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link TagService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDAO tagDAO;

    @Autowired
    private NewsDAO newsDAO;

    private static final Logger LOGGER = LogManager.getLogger(TagServiceImpl.class);

    /**
     * Implementation of {@link TagService#findAll()} method.
     */
    @Override
    public List<Tag> findAll() throws ServiceException{
        List<Tag> tags;
        try {
            tags = tagDAO.loadAll();
        }catch (Exception e) {
            LOGGER.error("Failed to load all Tags",e);
            throw new ServiceException(e);
        }
        return tags;
    }

    /**
     * Implementation of {@link TagService#findById(Long)}} method.
     */
    @Override
    public Tag findById(Long id) throws ServiceException{
        Tag tag;
        try {
            tag = tagDAO.loadOneById(id);
        }catch (Exception e) {
            LOGGER.error("Failed to load Tag by id",e);
            throw new ServiceException(e);
        }
        return tag;
    }

    /**
     * Implementation of {@link TagService#create(Object)} method.
     */
    @Override
    public Long create(Tag tag) throws ServiceException{
        Long id ;
        try {
            id = tagDAO.create(tag);
        } catch (Exception e) {
            LOGGER.error("Failed to add Tag : "+tag,e);
            throw new ServiceException(e);
        }
        return id;
    }


    /**
     * Implementation of {@link TagService#findByNewsId(Long)}  method.
     */
    @Override
    public List<Tag> findByNewsId(Long newsId) throws ServiceException {
        List<Tag> tags;
        try {
            tags = tagDAO.loadByNewsId(newsId);
        }catch (Exception e) {
            LOGGER.error("Failed to load Tags with newsId = "+newsId,e);
            throw new ServiceException(e);
        }
        return tags;

    }

    /**
     * Implementation of {@link TagService#update(Object)} method.
     */
    @Override
    public void update(Tag tag) throws ServiceException{
        try {
            tagDAO.update(tag);
        } catch (Exception e){
            LOGGER.error("Failed to update tag:"+tag,e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link TagService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try {
            Tag tag = new Tag();
            tag.setTagId(id);
            tagDAO.delete(tag);
        } catch (Exception e) {
            LOGGER.error("Failed to delete user with id = "+id,e);
            throw new ServiceException(e);
        }
    }
}
