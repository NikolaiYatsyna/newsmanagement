package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link AuthorService}.
 */
@Service
@Transactional
public class AuthorServiceImpl implements AuthorService{

    @Autowired
    private AuthorDAO authorDAO;

    private static final Logger LOGGER = LogManager.getLogger(AuthorServiceImpl.class);

    /**
     * Implementation of {@link AuthorService#findNonExpired()} method.
     */
    @Override
    public List<Author> findNonExpired() throws ServiceException {
        try {
            return authorDAO.loadNonExpiredAuthors();
        } catch (Exception e) {
            LOGGER.error("Failed to load non-expired authors", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#findByName(String)} method.
     */
    @Override
    public Author findByName(String name) throws ServiceException {
        try {
            return authorDAO.loadByName(name);
        } catch (Exception e) {
            LOGGER.error("Failed to load author with name = " + name, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#findById(Long)} method.
     */
    @Override
    public Author findById(Long id) throws ServiceException {
        try {
            return authorDAO.loadOneById(id);
        } catch (Exception e) {
            LOGGER.error("Failed to load author with id = " + id, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#update(Object)} method.
     */
    @Override
    public void update(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (Exception e) {
            LOGGER.error("Failed to update author: " + author, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try{
            Author author = new Author();
            author.setAuthorId(id);
            authorDAO.delete(author);
        } catch (Exception e) {
        LOGGER.error("Failed to delete author with " + id, e);
        throw new ServiceException(e);
    }
    }

    /**
     * Implementation of {@link AuthorService#create(Object)} method.
     */
    @Override
    public Long create(Author author) throws ServiceException {
        try {
            return authorDAO.create(author);
        }catch (Exception e){
            LOGGER.error("Failed to create author: "+author, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#findAll()} method.
     */
    @Override
    public List<Author> findAll() throws ServiceException {
        try {
            return authorDAO.loadAll();
        } catch (Exception e) {
            LOGGER.error("Failed to find authors", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#findByNewsId(Long)} method.
     */
    @Override
    public List<Author> findByNewsId(Long newsId) throws ServiceException {
        try {
            return authorDAO.loadByNewsId(newsId);
        } catch (Exception e) {
            LOGGER.error("Failed to load authors with news id = " + newsId, e);
            throw new ServiceException(e);
        }
    }
}
