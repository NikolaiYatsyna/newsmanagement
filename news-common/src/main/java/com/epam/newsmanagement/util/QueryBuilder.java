package com.epam.newsmanagement.util;

import com.epam.newsmanagement.domain.criteria.SearchCriteria;

import java.util.List;

/***
 * Query builder for implementation search using {@link SearchCriteria}.
 */
public class QueryBuilder {
    private static final String FIND_ALL = "SELECT DISTINCT NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE,NWS_MODIFICATION_DATE,NWS_VERSION FROM NEWS " +
            " INNER JOIN NEWS_AUTHORS ON NWS_ID=NAT_NEWS_ID INNER JOIN AUTHORS ON NAT_ATR_ID = ATR_ID" +
            " INNER JOIN NEWS_TAGS ON NWS_ID = NWT_NEWS_ID INNER JOIN TAGS ON NWT_TAG_ID = TAG_ID WHERE ";
    private static final String AUTHOR_ID = "ATR_ID";
    private static final String AND = " AND ";
    private static final String IN = " IN ";
    private static final String TAG_ID= " TAG_ID ";
    private static final String COMMA = ",";
    private static final String BRACKET_CLOSE = ")";
    private static final String BRACKET_OPEN = "(";
    private static final String HAVING_COUNT = " HAVING COUNT (DISTINCT ";
    private static final String EQUALS = "=";
    private static final String GROUP_BY =" GROUP BY NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE,NWS_MODIFICATION_DATE,NWS_VERSION";

    /**
     * Creates SQL query using to search.
     * @param searchCriteria {@link SearchCriteria} object which contains parameters of search.
     * @return String representation of SQL query.
     */
    public static String build(SearchCriteria searchCriteria){
        StringBuilder query = new StringBuilder();
        query.append(FIND_ALL);
        List<Long> authors =searchCriteria.getAuthorId();
        if(!authors.isEmpty()) {
            inClause(query,authors,AUTHOR_ID);
        }
        query.append(AND);
        List<Long> tags =searchCriteria.getTagsId();
        if(!tags.isEmpty()){
            inClause(query,tags,TAG_ID);
            havingCountClause(query,tags.size());
        }
        query.append(GROUP_BY);
        return query.toString();
    }

    private static void inClause(StringBuilder query, List<Long> items, String idName){
        query.append(idName);
        query.append(IN);
        query.append(BRACKET_OPEN);
        for (Long id : items){
            query.append(id);
            query.append(COMMA);
        }
        query.delete(query.length()-1,query.length());
        query.append(BRACKET_CLOSE);
    }

    private static void havingCountClause(StringBuilder query,int size){
        query.append(HAVING_COUNT);
        query.append(TAG_ID);
        query.append(BRACKET_CLOSE);
        query.append(EQUALS);
        query.append(size);
    }
}
