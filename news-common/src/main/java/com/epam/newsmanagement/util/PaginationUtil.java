package com.epam.newsmanagement.util;

import java.util.ResourceBundle;

public class PaginationUtil {
    private static Long newsPerPage;
    private static final Long DEFAULT_NEWS_PER_PAGE = 3L;

    static  {
        try {
            newsPerPage = Long.parseLong(ConfigurationManager.getNodesCount());
        }catch (Exception e){
            newsPerPage = DEFAULT_NEWS_PER_PAGE;
        }
    }

    public static Long calculatePagesCount(Long newsCount){
        if(newsCount%newsPerPage > 0){
            return newsCount/newsPerPage + 1;
        } else {
            return newsCount/newsPerPage;
        }
    }
    public static  Long calculateFirstIndex(Long pageNumber){
        return ((pageNumber-1) * newsPerPage) + 1 ;
    }

    public static Long calculateLastIndex(Long pageNumber){
        return ((pageNumber-1) * newsPerPage) + newsPerPage;
    }


    private static class ConfigurationManager{
        private static final String BUNDLE_NAME = "pagination";
        private static final String NODES_COUNT = "COUNT_OF_NODES";
        private static ResourceBundle resourceBundle;

        static {
            resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
        }

        public static String getNodesCount(){
            return (String) resourceBundle.getObject(NODES_COUNT);
        }
    }
}
