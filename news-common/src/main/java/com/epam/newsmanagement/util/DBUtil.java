package com.epam.newsmanagement.util;
import com.epam.newsmanagement.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;

/**
 * Util class for closing statement and resultSet objects and returing connetion into connection pool.
 */
public class DBUtil {

    private static final Logger LOGGER = LogManager.getLogger(DBUtil.class);

    /**
     * Closes statement and resultSet and returns connection into pool.
     * @param dataSource datasource object with whom programm is working.
     * @param connection connection object that is need to be released to pool.
     * @param statement statement object that is need to be closed.
     * @param resultSet resultSet object that is need to be closed;
     * @throws DAOException if there were errors while closing resultSet ,statement and returning connection into pool.
     */
    public static void closeAll(DataSource dataSource, Connection connection, Statement statement, ResultSet resultSet) throws DAOException {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                DataSourceUtils.doReleaseConnection(connection, dataSource);
            }
        } catch (SQLException e) {
            LOGGER.error("Exception caught while closing preparedStatement & resultSet");
            throw new DAOException(e);
        }

    }

    /**
     * Closes statement and returns connection into pool.
     * @param dataSource datasource object with whom programm is working.
     * @param connection connection object that is need to be released to pool.
     * @param statement statement object that is need to be closed.
     * @throws DAOException if there were errors while closing statement and returning connection into pool.
     */
    public static void closeAll(DataSource dataSource, Connection connection, Statement statement) throws DAOException {
        try {
            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                DataSourceUtils.doReleaseConnection(connection, dataSource);
            }
        } catch (SQLException e) {
            LOGGER.error("Exception caught while closing preparedStatement & resultSet");
            throw new DAOException(e);
        }
    }
}
