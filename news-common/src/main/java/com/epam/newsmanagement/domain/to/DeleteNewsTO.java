package com.epam.newsmanagement.domain.to;

import java.util.ArrayList;
import java.util.List;

public class DeleteNewsTO {
    private List<Long> deleteNewsList;

    public DeleteNewsTO(){
        deleteNewsList = new ArrayList<>();
    }

    public List<Long> getDeleteNewsList() {
        return deleteNewsList;
    }

    public void setDeleteNewsList(List<Long> deleteNewsList) {
        this.deleteNewsList = deleteNewsList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeleteNewsTO{");
        sb.append("deleteNewsList=").append(deleteNewsList);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeleteNewsTO that = (DeleteNewsTO) o;

        return deleteNewsList != null ? deleteNewsList.equals(that.deleteNewsList) : that.deleteNewsList == null;

    }

    @Override
    public int hashCode() {
        return deleteNewsList != null ? deleteNewsList.hashCode() : 0;
    }
}
