package com.epam.newsmanagement.domain.to;

import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.domain.bean.Tag;

import java.util.List;

public class AuthorTagTO {
    private List<Author> authors;
    private List<Tag> tags;

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Tag> getTags() {
        return tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthorTagTO that = (AuthorTagTO) o;

        if (authors != null ? !authors.equals(that.authors) : that.authors != null) return false;
        return tags != null ? tags.equals(that.tags) : that.tags == null;

    }

    @Override
    public int hashCode() {
        int result = authors != null ? authors.hashCode() : 0;
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AuthorTagTO{");
        sb.append("authors=").append(authors);
        sb.append(", tags=").append(tags);
        sb.append('}');
        return sb.toString();
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
