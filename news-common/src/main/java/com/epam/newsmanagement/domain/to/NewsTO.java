package com.epam.newsmanagement.domain.to;

import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.bean.Tag;

import java.util.List;

/**
 * Container for news , news authorList,list of news tags, list of news comments.
 */
public class NewsTO {
    private News news;
    private List<Author> authorList;
    private List<Tag> tagList;
    private List<Comment> commentList;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsTO newsTO = (NewsTO) o;

        if (news != null ? !news.equals(newsTO.news) : newsTO.news != null) return false;
        if (authorList != null ? !authorList.equals(newsTO.authorList) : newsTO.authorList != null) return false;
        if (tagList != null ? !tagList.equals(newsTO.tagList) : newsTO.tagList != null) return false;
        return commentList != null ? commentList.equals(newsTO.commentList) : newsTO.commentList == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (authorList != null ? authorList.hashCode() : 0);
        result = 31 * result + (tagList != null ? tagList.hashCode() : 0);
        result = 31 * result + (commentList != null ? commentList.hashCode() : 0);
        return result;
    }

    @Override
    public String
    toString() {
        final StringBuilder sb = new StringBuilder("NewsTO{");
        sb.append("news=").append(news);
        sb.append(", authorList=").append(authorList);
        sb.append(", tagList=").append(tagList);
        sb.append(", commentList=").append(commentList);
        sb.append('}');
        return sb.toString();
    }
}
