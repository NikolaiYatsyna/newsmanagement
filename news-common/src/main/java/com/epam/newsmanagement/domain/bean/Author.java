package com.epam.newsmanagement.domain.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Represents Author model for table AUTHORS in database.
 */
public class Author implements Serializable{
    private static final long serialVersionUID = 1L;
    private Long authorId;
    private String name;
    private Timestamp expired;

    private Set<News> news;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (authorId != null ? !authorId.equals(author.authorId) : author.authorId != null) return false;
        if (name != null ? !name.equals(author.name) : author.name != null) return false;
        return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

    }

    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Author{");
        sb.append("authorId=").append(authorId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", expired=").append(expired);
        sb.append('}');
        return sb.toString();
    }
}
