package com.epam.newsmanagement.domain.to;

import java.util.ArrayList;
import java.util.List;

public class DeleteCommentTO {
    private List<Long> deleteCommentList;

    public DeleteCommentTO() {
        deleteCommentList = new ArrayList<>();
    }

    public List<Long> getDeleteCommentList() {
        return deleteCommentList;
    }

    public void setDeleteCommentList(List<Long> deleteCommentList) {
        this.deleteCommentList = deleteCommentList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeleteCommentTO{");
        sb.append("deleteCommentList=").append(deleteCommentList);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeleteCommentTO that = (DeleteCommentTO) o;

        return deleteCommentList != null ? deleteCommentList.equals(that.deleteCommentList) : that.deleteCommentList == null;

    }

    @Override
    public int hashCode() {
        return deleteCommentList != null ? deleteCommentList.hashCode() : 0;
    }
}
