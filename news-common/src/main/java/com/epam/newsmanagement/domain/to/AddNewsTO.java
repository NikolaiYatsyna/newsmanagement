package com.epam.newsmanagement.domain.to;

import com.epam.newsmanagement.domain.bean.News;

import java.util.List;

public class AddNewsTO {
    private News news;
    private List<Long> authors;
    private List<Long> tags;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Long> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Long> authors) {
        this.authors = authors;
    }

    public List<Long> getTags() {
        return tags;
    }

    public void setTags(List<Long> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddNewsTO addNewsTO = (AddNewsTO) o;

        if (news != null ? !news.equals(addNewsTO.news) : addNewsTO.news != null) return false;
        if (authors != null ? !authors.equals(addNewsTO.authors) : addNewsTO.authors != null) return false;
        return tags != null ? tags.equals(addNewsTO.tags) : addNewsTO.tags == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AddNewsTO{");
        sb.append("news=").append(news);
        sb.append(", authors=").append(authors);
        sb.append(", tags=").append(tags);
        sb.append('}');
        return sb.toString();
    }
}
