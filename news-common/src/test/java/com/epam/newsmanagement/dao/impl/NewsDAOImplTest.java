package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test-config.xml")
@ActiveProfiles("hibernate")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagement.dao/NewsDAOImplTest.xml",type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value ="classpath:com.epam.newsmanagement.dao/NewsDAOImplTest.xml",type= DatabaseOperation.DELETE_ALL)

public class NewsDAOImplTest {

    @Autowired
    private NewsDAO newsDAO;
    @Autowired
    private CommentDAO commentDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }
    @Transactional
    @Test
    public void findAllTest(){
        final int expectedSize = 2;
        final int actualSize = newsDAO.loadAll().size();
        assertEquals(expectedSize,actualSize);
    }
    @Transactional
    @Test
    public void findNext(){
        final Long id = 1L;
        Long next = newsDAO.findNext(2L);
        assertEquals(next,id);
    }
    @Transactional
    @Test
    public void findOneTest(){
        final Long expectedId = 1L;
        News news = newsDAO.loadOneById(expectedId);
        Long actualId = news.getNewsId();
        assertEquals(expectedId,actualId);
    }
    @Transactional
    @Test
    public void createTest(){
        final String TITLE = "News test title";
        final String SHORT_TEXT = "Short test text";
        final String FULL_TEXT = "Full test text of news";
        final String CREATION_DATE_STRING = "2016-04-11 08:58:03.628";
        final String MODIFICATION_DATE_STRING = "2016-04-12";
        News news  = new News();
        news.setTitle(TITLE);
        news.setShortText(SHORT_TEXT);
        news.setFullText(FULL_TEXT);
        news.setCreationDate(Timestamp.valueOf(CREATION_DATE_STRING));
        news.setModificationDate(Date.valueOf(MODIFICATION_DATE_STRING));
        Long id = newsDAO.create(news);
        news.setNewsId(id);
        News actualNews = newsDAO.loadOneById(id);
        assertEquals(news,actualNews);
    }
    @Transactional
    @Test
    public void deleteTest(){
        News news = newsDAO.loadOneById(1L);
        newsDAO.delete(news);
    }
    @Transactional
    @Test
    public void updateTest(){
        final Long ID = 1L;
        final String TITLE = "Changed News test title ";
        News news = newsDAO.loadOneById(ID);
        news.setTitle(TITLE);
        newsDAO.update(news);
        News actualNews = newsDAO.loadOneById(ID);
        assertEquals(news,actualNews);
    }
    @Transactional
    @Test
    public void findByCriteriaTest(){
        final Long id = 1L;
        final int size = 1;
        SearchCriteria searchCriteria = new SearchCriteria();
        List<Long> authors = new ArrayList<>();
        authors.add(1l);
        searchCriteria.setAuthorId(authors);
        List<Long> tagsId = new ArrayList<>();
        tagsId.add(id);
        searchCriteria.setTagsId(tagsId);
        List<News> newsList = newsDAO.loadByCriteria(searchCriteria);
        assertEquals(size,newsList.size());
    }

    @Transactional
    @Test
    public void countNewsTest(){
        final Long expectedNewsCount = 2L;
        final Long actualNewsCount = newsDAO.countNews();
        assertEquals(expectedNewsCount,actualNewsCount);
    }
}
