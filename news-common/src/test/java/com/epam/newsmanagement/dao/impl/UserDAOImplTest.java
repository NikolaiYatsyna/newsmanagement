package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.bean.Role;
import com.epam.newsmanagement.domain.bean.User;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test-config.xml")
@ActiveProfiles("hibernate")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagement.dao/UserDAOImplTest.xml",type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value ="classpath:com.epam.newsmanagement.dao/UserDAOImplTest.xml",type= DatabaseOperation.DELETE_ALL)
@Transactional(readOnly = false)
public class UserDAOImplTest {
    @Autowired
    private UserDAO userDAO;
    @Test
    public void findByLoginTest(){
        final String login = "Nikolai";
        User user = userDAO.loadByLogin(login);
        assertEquals(login,user.getLogin());
    }

    @Test
    public void findAllTest(){
        final int EXPECTED_SIZE = 3;
        int size = userDAO.loadAll().size();
        assertEquals(EXPECTED_SIZE,size);
    }

    @Test
    public void findOneTest(){
        final Long ID = 1L;
        User user = userDAO.loadOneById(ID);
        assertEquals(ID,user.getUserId());
    }

    @Test
    public void deleteTest(){
        final Long ID = 1L;
        User user = new User();
        user.setUserId(ID);
        userDAO.delete(user);
    }

    @Test
    public void updateTest(){
        final Long ID = 2L;
        final Long NEW_ROLE_ID = 1L;
        final Role ROLE = new Role();
        final String ROLE_NAME="ROLE_ADMIN";
        Set<Role> roles = new HashSet<>();
        ROLE.setRoleId(NEW_ROLE_ID);
        ROLE.setName(ROLE_NAME);
        roles.add(ROLE);
        User user = userDAO.loadOneById(ID);
        user.setRole(ROLE);
        userDAO.update(user);
        User actualUser = userDAO.loadOneById(ID);
        assertEquals(user,actualUser);
    }

    @Test
    public void createTest(){
        final String NAME = "Sergei";
        final String LOGIN = "Sergei";
        final String PASSWORD = "qw12efgd";
        final Long NEW_ROLE_ID = 1L;
        final Role ROLE = new Role();
        final String ROLE_NAME="ROLE_ADMIN";
        ROLE.setRoleId(NEW_ROLE_ID);
        ROLE.setName(ROLE_NAME);
        User user = new User();
        user.setName(NAME);
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        user.setRole(ROLE);
        Long id = userDAO.create(user);
        user.setUserId(id);
        User actualUser = userDAO.loadOneById(id);
        assertEquals(user,actualUser);
    }





}
