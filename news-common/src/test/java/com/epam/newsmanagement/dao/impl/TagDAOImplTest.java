package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.bean.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test-config.xml")
@ActiveProfiles("hibernate")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagement.dao/TagDAOImplTest.xml",type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value ="classpath:com.epam.newsmanagement.dao/TagDAOImplTest.xml",type = DatabaseOperation.DELETE_ALL)
@Transactional(readOnly = false)
public class TagDAOImplTest {

    @Autowired
    private TagDAO tagDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void findAllTest(){
        final int expectedSize = 5;
        List<Tag> tags = tagDAO.loadAll();
        int actualSize = tags.size();
        assertEquals(expectedSize,actualSize);
    }
    @Test
    public void findOneTest(){
        final String EXPECTED_TAG_NAME = "sport";
        final Long id = 1L;
        Tag tag = tagDAO.loadOneById(id);
        assertEquals(EXPECTED_TAG_NAME,tag.getName());
    }
    @Test
    public void createTest(){
        final String TAG_NAME = "HEALTH";
        Tag tag = new Tag();
        tag.setName(TAG_NAME);
        Long id = tagDAO.create(tag);
        tag.setTagId(id);
        Tag actualTag = tagDAO.loadOneById(id);
        assertEquals(tag,actualTag);

    }
    @Test
    public void updateTest(){
        final Long id = 2L;
        final String NEW_TAG_NAME = "health";
        Tag tag = tagDAO.loadOneById(id);
        tag.setName(NEW_TAG_NAME);
        tagDAO.update(tag);
        Tag actualTag = tagDAO.loadOneById(id);
        assertEquals(tag,actualTag);
    }

    @Test
    public void deleteTest(){
        final Long id = 2L;
        Tag tag = new Tag();
        tag.setTagId(id);
        tagDAO.delete(tag);

    }


    @Test
    public void findByNewsIdTest(){
        final Long newsId = 1L;
        List<Tag> tags = tagDAO.loadByNewsId(newsId);
        assertNotEquals(tags, Collections.emptyList());
    }
}
