package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.bean.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test-config.xml")
@ActiveProfiles("hibernate")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagement.dao/CommentDAOImplTest.xml",type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value ="classpath:com.epam.newsmanagement.dao/CommentDAOImplTest.xml",type= DatabaseOperation.DELETE_ALL)
@Transactional
public class CommentDAOImplTest {

    @Autowired
    private  CommentDAO commentDao;

    @Autowired
    private NewsDAO newsDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }
    @Test
    public void findAllTest(){
        final int size=4;
        List<Comment> commentList = commentDao.loadAll();
        assertEquals(size,commentList.size());
    }
    @Test
    public void findOneTest(){
        final Long id = 1L;
        Comment comment = commentDao.loadOneById(id);
        assertEquals(id,comment.getCommentId());
    }
    @Test
    public void findByNewsIdTest(){
        final Long newsId = 1L;
        final int size = 2;
        List<Comment> comments = commentDao.loadByNewsId(newsId);
        assertEquals(size,comments.size());
    }

    @Test
    public void delete(){
        final Long id = 1L;
        Comment comment = new Comment();
        comment.setCommentId(id);
        commentDao.delete(comment);
    }

    @Test
    public void deleteAll(){
        final Long id = 1L;
        Comment comment = new Comment();
        comment.setCommentId(id);
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        commentDao.deleteAll(commentList);
    }

    @Test
    public void deleteByNewsId(){
        final Long id = 1L;
        commentDao.deleteByNewsId(id);
    }

    @Test
    public void updateTest(){
        final String NEW_TEXT ="Changed Text";
        final Long ID = 1L;
        Comment comment = commentDao.loadOneById(ID);
        comment.setText(NEW_TEXT);
        commentDao.update(comment);
        Comment actual = commentDao.loadOneById(ID);
        assertEquals(comment,actual);
    }
    @Test
    public void createTest(){
        Comment comment = buildComment();
        News news = newsDAO.loadOneById(1L);
        comment.setNews(news);
        Long id = commentDao.create(comment);

        comment.setCommentId(id);
        Comment actualComment = commentDao.loadOneById(id);
        assertEquals(comment.getCommentId(),actualComment.getCommentId());
    }


    private Comment buildComment(){
        final String COMMENT_TEXT = "Comment 2 text";
        final String CREATION_DATE_TEXT = "2016-02-18 08:58:03.628";
        Comment comment = new Comment();
        comment.setText(COMMENT_TEXT);
        comment.setCreationDate(Timestamp.valueOf(CREATION_DATE_TEXT));
        return comment;
    }

}
