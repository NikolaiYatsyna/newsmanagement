package com.epam.newsmanagement.dao.impl;
import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.bean.Role;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test-config.xml")
@ActiveProfiles("hibernate")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagement.dao/RoleDAOImplTest.xml",type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value ="classpath:com.epam.newsmanagement.dao/RoleDAOImplTest.xml",type= DatabaseOperation.DELETE_ALL)
@Transactional(readOnly = false)
public class RoleDAOImplTest {

    @Autowired
    private RoleDAO roleDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void findAllTest(){
        final int expectedSize = 2;
        final int actualSize = roleDAO.loadAll().size();
        assertEquals(expectedSize,actualSize);
    }

    @Test
    public void findOneTest(){
        final Long id = 1L;
        final String expectedName = "ROLE_ADMIN";
        Role role = roleDAO.loadOneById(id);
        assertEquals(expectedName,role.getName());
    }

    @Test
    public void createTest(){
        final String ROLE_NAME = "ROLE_USER";
        Role role = new Role();
        role.setName(ROLE_NAME);
        Long id = roleDAO.create(role);
        role.setRoleId(id);
        Role actualRole = roleDAO.loadOneById(id);
        assertEquals(role,actualRole);
    }

    @Test
    public void deleteTest(){
        final Long id= 1L;
        Role role = new Role();
        role.setRoleId(id);
        roleDAO.delete(role);
    }

    public void updateTest(){
        final String ROLE_NAME = "ROLE_USER";
        final Long id = 2L;
        Role expectedRole = roleDAO.loadOneById(id);
        expectedRole.setName(ROLE_NAME);
        roleDAO.update(expectedRole);
        Role actualRole = roleDAO.loadOneById(id);
        assertEquals(expectedRole,actualRole);
    }
}
