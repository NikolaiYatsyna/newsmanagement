package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.bean.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test-config.xml")
@ActiveProfiles("hibernate")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagement.dao/AuthorDAOImplTest.xml",type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value ="classpath:com.epam.newsmanagement.dao/AuthorDAOImplTest.xml",type= DatabaseOperation.DELETE_ALL)
@Transactional(readOnly = false)
public class AuthorDAOImplTest {

    @Autowired
    private AuthorDAO authorDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void findAllTest(){
        final int expectedSize = 4;
        List<Author> authors = authorDAO.loadAll();
        int actualSize = authors.size();
        assertEquals(expectedSize,actualSize);
    }
    @Test
    public void findOneTest(){
        final Long id = 1L;
        Author author = authorDAO.loadOneById(id);
        assertEquals(id,author.getAuthorId());
    }
    @Test
    public void createTest(){
        final String NAME = "Vasya";
        //final String EXPIRED_STRING = "2016-01-18 08:58:03.628";
        Author author = new Author();
        author.setName(NAME);
        author.setExpired(null);
        Long actualId = authorDAO.create(author);
        author.setAuthorId(actualId);
        Author actual = authorDAO.loadOneById(actualId);
        assertEquals(author,actual);
    }
    @Test
    public void updateTest(){
        final String EXPIRED_STRING = "2016-05-18 08:58:03.628";
        final Long id = 1L;
        final Timestamp newTimestamp = Timestamp.valueOf(EXPIRED_STRING);
        Author author = authorDAO.loadOneById(id);
        author.setExpired(newTimestamp);
        authorDAO.update(author);
        author = authorDAO.loadOneById(id);
        assertEquals(newTimestamp,author.getExpired());
    }

    @Test
    public void deleteTest(){
        final Long id = 4L;
        Author author = new Author();
        author.setAuthorId(id);
        authorDAO.delete(author);
    }
    @Test
    public void findNonExpiredAuthorsTest(){
        final int nonExpiredAuthorsCount = 0;
        List<Author> authors = authorDAO.loadNonExpiredAuthors();
        assertEquals(nonExpiredAuthorsCount,authors.size());
    }
    @Test
    public void findByNameTest(){
        final String AUTHOR_NAME = "Nikolai";
        String actualName = authorDAO.loadByName(AUTHOR_NAME).getName();
        assertEquals(AUTHOR_NAME,actualName);
    }
    @Test
    public void findByNewsIdTest(){
        final Long newsId = 1L;
        final Long authorId = 1L;
        List<Author> author = authorDAO.loadByNewsId(newsId);
        assertEquals(authorId,author.get(0).getAuthorId());
    }
}
