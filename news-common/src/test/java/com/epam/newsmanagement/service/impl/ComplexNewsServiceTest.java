package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.domain.to.DeleteNewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ComplexNewsServiceTest {

    @Mock
    NewsServiceImpl newsService;

    @Mock
    TagServiceImpl tagService;

    @Mock
    AuthorServiceImpl authorService;

    @Mock
    CommentServiceImpl commentService;


    @InjectMocks
    ComplexNewsServiceImpl complexNewsService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createNewsWithTagsAndAuthorTest() throws ServiceException {
        final Long newsId = 1L;
        final Author author = new Author();
        final List<Long> tagsId = Collections.EMPTY_LIST;
        final List<Long> authors = Collections.EMPTY_LIST;

        final News news  = new News();
        final List<Tag> tags = new ArrayList<>();

        when(newsService.create(news)).thenReturn(newsId);
        Long actualId = complexNewsService.createNewsWithTagsAndAuthor(news,authors,tagsId);
        assertEquals(newsId,actualId);
        verify(newsService).create(news);
    }


    @Test
    public void deleteNewsWithTagsAndAuthorTest() throws ServiceException{
        final Long[] newsId = {1L};
        DeleteNewsTO deleteNewsTO = new DeleteNewsTO();
        List<Long> id = Collections.EMPTY_LIST;
        deleteNewsTO.setDeleteNewsList(id);
        newsService.delete(newsId);
        complexNewsService.deleteNewsWithTagsAndAuthor(deleteNewsTO);
        verify(newsService).delete(newsId);

    }

    @Test
    public void findNewsTOTest() throws ServiceException{
        final Long newsId = 1L;
        final List<Tag> tags = Collections.EMPTY_LIST;
        final List<Author> author = Collections.EMPTY_LIST;
        final News news  = new News();
        when(newsService.findById(newsId)).thenReturn(news);
        when(tagService.findByNewsId(newsId)).thenReturn(tags);
        when(authorService.findByNewsId(newsId)).thenReturn(author);

        complexNewsService.findNewsTO(newsId);

        verify(newsService).findById(newsId);
        verify(tagService).findByNewsId(newsId);
        verify(authorService).findByNewsId(newsId);

    }
}
