package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.bean.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
public class TagServiceImplTest {

    @Mock
    private TagDAO tagDAO;
    @Mock
    private NewsDAO newsDAO;

    @InjectMocks
    TagServiceImpl tagService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTest() throws ServiceException {
        final Tag tag = new Tag();
        final Long id = 1L;
        when(tagDAO.create(tag)).thenReturn(id);
        Long actualId = tagService.create(tag);
        assertEquals(id,actualId);
        verify(tagDAO).create(tag);
    }

    @Test
    public void findAllTest() throws ServiceException{
        final List<Tag> tags = Collections.EMPTY_LIST;
        when(tagDAO.loadAll()).thenReturn(tags);
        List<Tag> actualTagList = tagService.findAll();
        assertEquals(tags,actualTagList);
        verify(tagDAO).loadAll();
    }

    @Test
    public void findByIdTest() throws ServiceException{
        final Long id = 1L;
        final Tag tag = new Tag();
        when(tagDAO.loadOneById(id)).thenReturn(tag);
        Tag actualTag = tagService.findById(id);
        assertEquals(tag,actualTag);
        verify(tagDAO).loadOneById(id);
    }

    @Test
    public void findByNewsIdTest() throws ServiceException{
        final Long id = 1L;
        List<Tag> tags = Collections.EMPTY_LIST;
        when(tagDAO.loadByNewsId(id)).thenReturn(tags);
        List<Tag> actualTags = tagService.findByNewsId(id);
        assertEquals(tags,actualTags);
        verify(tagDAO).loadByNewsId(id);
    }

    @Test
    public void createTagsTest() throws ServiceException{
        final Tag tag = new Tag();
        final Long id = 1L;
        when(tagDAO.create(tag)).thenReturn(id);
        Long actualId = tagService.create(tag);
        assertEquals(id,actualId);
        verify(tagDAO).create(tag);
    }

    @Test
    public void updateTest() throws ServiceException{
        final Tag tag = new Tag();
        tagService.update(tag);
        verify(tagDAO).update(tag);
    }

    @Test
    public void deleteTest() throws ServiceException{
        final Long[] id = {1L};
        final Tag tag = new Tag();
        tag.setTagId(id[0]);
        tagService.delete(id[0]);
        verify(tagDAO).delete(tag);
    }
}
