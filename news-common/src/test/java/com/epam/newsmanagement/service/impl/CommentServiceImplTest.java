package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.bean.Comment;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CommentServiceImplTest {

    @Mock
    CommentDAO commentDAO;

    @InjectMocks
    CommentServiceImpl commentService;


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void create() throws ServiceException{
        final Comment comment = new Comment();
        final Long id = 1L;
        when(commentDAO.create(comment)).thenReturn(id);
        Long actualId = commentService.create(comment);
        assertEquals(id,actualId);
        verify(commentDAO).create(comment);
    }


    @Test
    public void findAllTest() throws ServiceException{
        List<Comment> comments = Collections.EMPTY_LIST;
        when(commentDAO.loadAll()).thenReturn(comments);
        List<Comment> actualCommentList = commentService.findAll();
        assertEquals(comments,actualCommentList);
        verify(commentDAO).loadAll();
    }

    @Test
    public void findByNewsId() throws ServiceException{
        final List<Comment> comments = Collections.EMPTY_LIST;
        final Long id = 1L;
        when(commentDAO.loadByNewsId(id)).thenReturn(comments);
        List<Comment> actualComments = commentService.findByNewsId(id);
        assertEquals(comments,actualComments);
        verify(commentDAO).loadByNewsId(id);
    }

    @Test
    public void delete() throws ServiceException{
        final Long id = 1L;
        final Comment comment = new Comment();
        comment.setCommentId(id);
        commentService.delete(id);
        verify(commentDAO).delete(comment);
    }


    @Test
    public void update() throws ServiceException{
        final Comment comment = new Comment();
        commentService.update(comment);
        verify(commentDAO).update(comment);

    }


    @Test
    public void findById() throws ServiceException{
        final Comment comment = new Comment();
        final Long id = 1L;
        when(commentDAO.loadOneById(id)).thenReturn(comment);
        Comment actualComment = commentService.findById(id);
        assertEquals(comment,actualComment);
        verify(commentDAO).loadOneById(id);
    }

    @Test
    public void deleteByNewsId() throws ServiceException{
        final Long newsId = 1L;
        final News news = new News();
        final Comment comment = new Comment();
        news.setNewsId(newsId);
        comment.setNews(news);
        commentService.deleteByNewsId(new Long[]{newsId});
        verify(commentDAO).deleteByNewsId(newsId);
    }
}
