package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.bean.User;
import com.epam.newsmanagement.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
public class UserServiceImplTest {

    @Mock
    UserDAO userDAO;

    @InjectMocks
    UserServiceImpl userService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTest() throws ServiceException{
        final User user = new User();
        final Long id = 1L;
        when(userDAO.create(user)).thenReturn(id);
        Long actualId = userService.create(user);
        assertEquals(id,actualId);
        verify(userDAO).create(user);
    }

    @Test
    public void findAllTest() throws ServiceException{
        final List<User> users = Collections.EMPTY_LIST;
        when(userDAO.loadAll()).thenReturn(users);
        List<User> actualUsersList = userService.findAll();
        assertEquals(users,actualUsersList);
        verify(userDAO).loadAll();
    }

    @Test
    public void findByIdTest() throws ServiceException{
        final User user = new User();
        final Long id = 1L;
        when(userDAO.loadOneById(id)).thenReturn(user);
        User actualUser = userService.findById(id);
        assertEquals(user,actualUser);
        verify(userDAO).loadOneById(id);
    }

    @Test
    public void findByLoginTest() throws ServiceException{
        final String login= "login";
        final User user = new User();
        when(userDAO.loadByLogin(login)).thenReturn(user);
        User actualUser = userService.findByLogin(login);
        assertEquals(user,actualUser);
        verify(userDAO).loadByLogin(login);
    }


    @Test
    public void updateTest() throws ServiceException{
        final User user = new User();
        userService.update(user);
        verify(userDAO).update(user);
    }


    @Test
    public void deleteTest() throws ServiceException{
        final Long id = 1L;
        final User user = new User();
        user.setUserId(id);
        userService.delete(id);
        verify(userDAO).delete(user);
    }
}
