package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.domain.bean.Role;
import com.epam.newsmanagement.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RoleServiceImplTest {
    @InjectMocks
    RoleServiceImpl roleService;

    @Mock
    RoleDAO roleDAO;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void loadByIdTest() throws ServiceException {
        Role role = new Role();
        final Long id = 1L;
        when(roleDAO.loadOneById(id)).thenReturn(role);
        Role actualRole  = roleService.findById(id);
        verify(roleDAO).loadOneById(id);
        assertEquals(role,actualRole);
    }


}
