package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.bean.News;
import com.epam.newsmanagement.domain.criteria.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class NewsServiceImplTest {
    @Mock
    private NewsDAO newsDAO;

    @InjectMocks
    private NewsServiceImpl newsService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTest() throws ServiceException {
        final News news = new News();
        final Long id = 1L;
        when(newsDAO.create(news)).thenReturn(id);
        Long actualId = newsService.create(news);
        assertEquals(id,actualId);
        verify(newsDAO).create(news);
    }

    @Test
    public void findAllTest() throws ServiceException {
        List<News> newsList = Collections.EMPTY_LIST;
        when(newsDAO.loadAll()).thenReturn(newsList);
        List<News> actualNewsList = newsService.findAll();
        assertEquals(newsList,actualNewsList);
        verify(newsDAO).loadAll();
    }

    @Test
    public void findByIdTest() throws ServiceException {
        final News news = new News();
        final Long newsId = 1L;
        when(newsDAO.loadOneById(newsId)).thenReturn(news);
        News actualNews = newsService.findById(newsId);
        assertEquals(news,actualNews);
        verify(newsDAO).loadOneById(newsId);
    }

    @Test
    public void deleteTest() throws ServiceException {
        final Long id = 1L;
        final News news = new News();
        news.setNewsId(id);
        newsService.delete(id);
        verify(newsDAO).delete(news);
    }

    @Test
    public void updateTest() throws ServiceException {
        final News news  = new News();
        newsService.update(news);
        verify(newsDAO).update(news);
    }

    @Test
    public void findByCriteriaTest() throws ServiceException {
        SearchCriteria criteria = new SearchCriteria();
        List<News> newsList = Collections.EMPTY_LIST;
        when(newsDAO.loadByCriteria(criteria)).thenReturn(newsList);
        List<News> actualNewsList = newsService.findByCriteria(criteria);
        assertEquals(newsList,actualNewsList);
        verify(newsDAO).loadByCriteria(criteria);
    }

    @Test
    public void countOfNodesTest() throws ServiceException {
        final Long count = 0L;
        when(newsDAO.countNews()).thenReturn(count);
        Long actualCount = newsService.countOfNodes();
        assertEquals(count,actualCount);
    }
}