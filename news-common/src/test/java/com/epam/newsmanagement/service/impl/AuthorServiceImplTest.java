package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.bean.Author;
import com.epam.newsmanagement.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
public class AuthorServiceImplTest {

    @Mock
    AuthorDAO authorDAO;

    @InjectMocks
    AuthorServiceImpl authorService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTest() throws ServiceException{
        final Author author = new Author();
        final Long id = 1L;

        when(authorDAO.create(author)).thenReturn(id);
        Long actualId = authorService.create(author);
        assertEquals(id,actualId);
        verify(authorDAO).create(author);

    }

    @Test
    public void findAllTest() throws ServiceException{
        final List<Author> authors = Collections.EMPTY_LIST;
        when(authorDAO.loadAll()).thenReturn(authors);
        List<Author> actualAuthorsList = authorService.findAll();
        assertEquals(authors,actualAuthorsList);
        verify(authorDAO).loadAll();
    }

    @Test
    public void findNonExpiredTest() throws ServiceException {
        final List<Author> authors = Collections.EMPTY_LIST;
        when(authorDAO.loadNonExpiredAuthors()).thenReturn(authors);
        List<Author> actualAuthorsList = authorService.findNonExpired();
        assertEquals(authors, actualAuthorsList);
        verify(authorDAO).loadNonExpiredAuthors();
    }

    @Test
    public void findByNameTest() throws ServiceException{
        final String name = "Name";
        final Author author = new Author();
        when(authorDAO.loadByName(name)).thenReturn(author);
        Author actualAuthor = authorService.findByName(name);
        assertEquals(author,actualAuthor);
        verify(authorDAO).loadByName(name);
    }


    @Test
    public void findByIdTest() throws ServiceException{
        final Long id = 1L;
        final Author author = new Author();
        when(authorDAO.loadOneById(id)).thenReturn(author);
        Author actualAuthor = authorService.findById(id);
        assertEquals(author,actualAuthor);
        verify(authorDAO).loadOneById(id);
    }


    @Test
    public void findByNewsIdTest() throws ServiceException{
        final Long newsId = 1L;
        final List<Author> authors = Collections.EMPTY_LIST;
        when(authorDAO.loadByNewsId(newsId)).thenReturn(authors);
        List<Author> actualAuthors = authorService.findByNewsId(newsId);
        assertEquals(authors,actualAuthors);
        verify(authorDAO).loadByNewsId(newsId);
    }


    @Test
    public void updateTest() throws ServiceException{
        final Author author = new Author();
        authorService.update(author);
        verify(authorDAO).update(author);
    }


    @Test
    public void deleteTest() throws ServiceException{
        final Long id = 1L;
        final Author author = new Author();
        author.setAuthorId(id);
        authorService.delete(id);
        verify(authorDAO).delete(author);
    }

}
