DROP TABLE "NEWS"."NEWS_AUTHORS";
DROP TABLE "NEWS"."NEWS_TAGS";
DROP TABLE "NEWS"."AUTHORS";
DROP TABLE "NEWS"."COMMENTS";
DROP TABLE "NEWS"."NEWS";
DROP TABLE "NEWS"."USERS";
DROP TABLE "NEWS"."ROLES";
DROP TABLE "NEWS"."TAGS";

DROP SEQUENCE "NEWS"."NWS_ID_SEQ";
DROP SEQUENCE "NEWS"."ATR_ID_SEQ";
DROP SEQUENCE "NEWS"."CMT_ID_SEQ";
DROP SEQUENCE "NEWS"."USR_ID_SEQ";
DROP SEQUENCE "NEWS"."ROLE_ID_SEQ";
DROP SEQUENCE "NEWS"."TAG_ID_SEQ";
--------------------------------------------------------
--  DDL for Table AUTHORS
--------------------------------------------------------

  CREATE TABLE "NEWS"."AUTHORS" ("ATR_ID" NUMBER(20,0), "ATR_NAME" NVARCHAR2(100), "ATR_EXPIRED" TIMESTAMP (6) DEFAULT NULL) ;
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "NEWS"."COMMENTS" ("CMT_ID" NUMBER(20,0), "CMT_TEXT" NVARCHAR2(500), "CMT_CREATION_DATE" TIMESTAMP (6), "CMT_NWS_ID" NUMBER(20,0)) ;
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "NEWS"."NEWS" ("NWS_ID" NUMBER(20,0), "NWS_TITLE" NVARCHAR2(500), "NWS_SHORT_TEXT" NVARCHAR2(1000), "NWS_FULL_TEXT" NVARCHAR2(2000), "NWS_CREATION_DATE" TIMESTAMP (6), "NWS_MODIFICATION_DATE" DATE, "NWS_VERSION" NUMBER) ;
--------------------------------------------------------
--  DDL for Table NEWS_AUTHORS
--------------------------------------------------------

  CREATE TABLE "NEWS"."NEWS_AUTHORS" ("NAT_NEWS_ID" NUMBER(20,0), "NAT_ATR_ID" NUMBER(20,0)) ;
--------------------------------------------------------
--  DDL for Table NEWS_TAGS
--------------------------------------------------------

  CREATE TABLE "NEWS"."NEWS_TAGS" ("NWT_NEWS_ID" NUMBER(20,0), "NWT_TAG_ID" NUMBER(20,0)) ;
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "NEWS"."ROLES" ("ROLE_ID" NUMBER(20,0), "ROLE_NAME" VARCHAR2(50)) ;
--------------------------------------------------------
--  DDL for Table TAGS
--------------------------------------------------------

  CREATE TABLE "NEWS"."TAGS" ("TAG_ID" NUMBER(20,0), "TAG_NAME" NVARCHAR2(200)) ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "NEWS"."USERS" ("USR_ID" NUMBER(20,0), "USR_NAME" NVARCHAR2(50), "USR_LOGIN" VARCHAR2(20), "USR_PASSWORD" VARCHAR2(32) DEFAULT NULL, "USR_ROLE_ID" NUMBER(20,0)) ;
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS"."AUTHOR_PK" ON "NEWS"."AUTHORS" ("ATR_ID") ;
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS"."COMMENTS_PK" ON "NEWS"."COMMENTS" ("CMT_ID") ;
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS"."NEWS_PK" ON "NEWS"."NEWS" ("NWS_ID") ;
--------------------------------------------------------
--  DDL for Index ROLES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS"."ROLES_PK" ON "NEWS"."ROLES" ("ROLE_ID") ;
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS"."TAG_PK" ON "NEWS"."TAGS" ("TAG_ID") ;
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS"."USERS_PK" ON "NEWS"."USERS" ("USR_ID") ;
--------------------------------------------------------
--  Constraints for Table AUTHORS
--------------------------------------------------------

  ALTER TABLE "NEWS"."AUTHORS" MODIFY ("ATR_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."AUTHORS" MODIFY ("ATR_NAME" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."AUTHORS" ADD CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("ATR_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "NEWS"."COMMENTS" MODIFY ("CMT_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."COMMENTS" MODIFY ("CMT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."COMMENTS" MODIFY ("CMT_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("CMT_ID") ENABLE;
  ALTER TABLE "NEWS"."COMMENTS" MODIFY ("CMT_NWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "NEWS"."NEWS" MODIFY ("NWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS" MODIFY ("NWS_TITLE" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS" MODIFY ("NWS_SHORT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS" MODIFY ("NWS_FULL_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS" MODIFY ("NWS_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS" MODIFY ("NWS_MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NWS_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "NEWS"."NEWS_AUTHORS" MODIFY ("NAT_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS_AUTHORS" MODIFY ("NAT_ATR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "NEWS"."NEWS_TAGS" MODIFY ("NWT_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."NEWS_TAGS" MODIFY ("NWT_TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "NEWS"."ROLES" MODIFY ("ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."ROLES" MODIFY ("ROLE_NAME" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."ROLES" ADD CONSTRAINT "ROLE_PK" PRIMARY KEY ("ROLE_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TAGS
--------------------------------------------------------

  ALTER TABLE "NEWS"."TAGS" MODIFY ("TAG_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."TAGS" MODIFY ("TAG_NAME" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."TAGS" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "NEWS"."USERS" MODIFY ("USR_ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."USERS" MODIFY ("USR_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."USERS" MODIFY ("USR_NAME" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."USERS" MODIFY ("USR_LOGIN" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."USERS" MODIFY ("USR_PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "NEWS"."USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USR_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "NEWS"."COMMENTS" ADD CONSTRAINT "CMT_NWS_FK" FOREIGN KEY ("CMT_NWS_ID") REFERENCES "NEWS"."NEWS" ("NWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "NEWS"."NEWS_AUTHORS" ADD CONSTRAINT "NAT_ATR_FK" FOREIGN KEY ("NAT_ATR_ID") REFERENCES "NEWS"."AUTHORS" ("ATR_ID") ENABLE;
  ALTER TABLE "NEWS"."NEWS_AUTHORS" ADD CONSTRAINT "NAT_NEWS_FK" FOREIGN KEY ("NAT_NEWS_ID") REFERENCES "NEWS"."NEWS" ("NWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "NEWS"."NEWS_TAGS" ADD CONSTRAINT "NWS_NEWS_FK" FOREIGN KEY ("NWT_NEWS_ID") REFERENCES "NEWS"."NEWS" ("NWS_ID") ENABLE;
  ALTER TABLE "NEWS"."NEWS_TAGS" ADD CONSTRAINT "NWS_TAG_FK" FOREIGN KEY ("NWT_TAG_ID") REFERENCES "NEWS"."TAGS" ("TAG_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "NEWS"."USERS" ADD CONSTRAINT "USR_ROLE_FK" FOREIGN KEY ("USR_ROLE_ID") REFERENCES "NEWS"."ROLES" ("ROLE_ID") ENABLE;

CREATE SEQUENCE  "ATR_ID_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CMT_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE  "CMT_ID_SEQ"  MINVALUE 1  INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NWS_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE  "NWS_ID_SEQ"  MINVALUE 1  INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAG_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE  "TAG_ID_SEQ"  MINVALUE 1  INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USR_ID_SEQ
--------------------------------------------------------

CREATE SEQUENCE  "USR_ID_SEQ"  MINVALUE 1  INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ROLE_ID_SEQ
--------------------------------------------------------
CREATE SEQUENCE  "ROLE_ID_SEQ"  MINVALUE 1  INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;