## Todo List: ##
1. Investigate linkedin api https://developer.linkedin.com/docs/rest-api
2. Replace user personalized timeline with old one predefined stream you have implemented in previous tasks (convert LinkedIn activities to news);

3. Make the authentication using OAuth 2.0 as Linkedin suggests;
4. Implement pagination with Ajax;
5. Delete all your server pages and replace it with single html file wired with js controllers for navigation proposals. Implement MVC on client side using Angular JS.
6. NewsManagement Testing application located [here](https://bitbucket.org/NikolaiYatsyna/newsmanagement-testing)
  
## Already implemented: ##
1. 4 dao implementations (Eclipse-link, Hibernate, Simple jdbc, Spring jdbc template).
2. Swtitching dao implementations with help of Spring profiles.
3. Optimistic Lock concept
5. Hibernate Caching technique (2-nd level cachr with ehcache)
6. Angular JS on the client side